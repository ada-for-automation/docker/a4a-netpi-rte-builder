/**************************************************************************************

   Copyright (c) Hilscher GmbH. All Rights Reserved.

 **************************************************************************************

   Filename:
    $Workfile: OS_Includes.h $
   Last Modification:
    $Author: Sebastiand $
    $Modtime: 20.01.09 13:32 $
    $Revision: 792 $

   Targets:
     Win32/ANSI   : yes

   Description:
    Header file for OS specific functions (-> OS_KMDF.c)

   Changes:

     Version   Date        Author   Description
     ----------------------------------------------------------------------------------
     2        05.02.2013   SD       add file header
     1        -/-          MT       initial version

**************************************************************************************/

#ifndef __NXT_OS_INCLUDES__H
#define __NXT_OS_INCLUDES__H

#include <math.h>
#include <linux/types.h>
#include <stdlib.h>

#undef SLIST_ENTRY
//#pragma warning(disable : 4127 )  /* Disable conditional expression is constant (used in TAILQ) */

#define APIENTRY

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef MIN
#define MIN(a,b) ((a < b)? (a) : (b))
#endif

#ifndef NULL
#define NULL (void*)(0)
#endif

#define PTR2LONG(x) (unsigned long)(x)
#define LONG2PTR(x) (void*)(x)

#ifndef UNREFERENCED_PARAMETER
#define UNREFERENCED_PARAMETER(x) ((void)(x))
#endif

#define OS_Min(x,y) (((x)<(y))?(x):(y))
#define OS_Max(x,y) (((x)>(y))?(x):(y))

#define OS_Abs(x) (abs(x))

typedef void (*pfThreadFunc)(void* pvParam);

typedef void *(*start_routine) (void *);

typedef struct LINUX_THREAD_PARAM_Ttag
{
  pthread_t              tThread;
  pthread_attr_t         tAttr;
  start_routine          pfnStart;
  void*                  pvArg;
  int                    iRet;
  pfThreadFunc           pfThreadFunction;
  void*                  pvUserParam;

} LINUX_THREAD_PARAM_T, *PLINUX_THREAD_PARAM_T;

#endif /*  __NXT_OS_INCLUDES__H */
