/**************************************************************************************

   Copyright (c) Hilscher GmbH. All Rights Reserved.

 **************************************************************************************

   Filename:
    $Id: User_win32.c 3194 2011-12-14 16:36:28Z Robert $
   Last Modification:
    $Author: Robert $
    $Date: 2011-12-14 17:36:28 +0100 (Mi, 14 Dez 2011) $
    $Revision: 3194 $

   Targets:
     unix        : yes

   Description:
     USER implemented functions called by the netXTransport Toolkit.

   Changes:

     Version   Date        Author   Description
     ----------------------------------------------------------------------------------
      1        23.02.13    SD       initial version

**************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <time.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <pthread.h>
#include "netXTransport.h"
#include "HilTransportLayer.h"
#include "OS_Dependent.h"

typedef struct NXT_LOG_FILE_Ttag
{
  void* hnetXTransport;
  FILE* hFile;
} NXT_LOG_FILE_T;

NETX_TRANSPORT_DATA_T* g_ptnetXTransportInst;


/*****************************************************************************/
/*! Print a trace message from netXtransport toolkit
*     \param ptDevInstance  Device instance the trace is coming from
*     \param ulTraceLevel   see TRACE_LVL_XXX defines
*     \param szFormat       printf style format string
*     \param ...            printf arguments                                 */
/*****************************************************************************/
void USER_Trace(NETX_TRANSPORT_DATA_T* ptnetXTransportInst, uint32_t ulTraceLevel, char* szFormat, ...)
{
  va_list                 vaList;
  struct timeval          time;
  struct tm               *local_tm;
  NXT_LOG_FILE_T* ptLogFile;

  /* TODO: fix when calling USER_Trace() out of HilTransportLayer */
  if (ptnetXTransportInst == NULL)
  {
    ptnetXTransportInst = g_ptnetXTransportInst;
  } else
  {
    g_ptnetXTransportInst = ptnetXTransportInst;
  }
  ptLogFile = (NXT_LOG_FILE_T*)ptnetXTransportInst->pvLogFile;

  UNREFERENCED_PARAMETER(ulTraceLevel);

  gettimeofday(&time, NULL);
  local_tm = localtime(&time.tv_sec);

  va_start(vaList, szFormat);

  if(ptLogFile && ptLogFile->hFile)
  {
    fprintf(ptLogFile->hFile,
              "%.2d.%.2d.%.4d %.2d:%.2d:%.2d.%.3ld.%.3ld: ",
              local_tm->tm_mday, local_tm->tm_mon + 1, local_tm->tm_year + 1900,
              local_tm->tm_hour, local_tm->tm_min, local_tm->tm_sec,
              time.tv_usec / 1000, time.tv_usec % 1000);

    /* log file is given, so add this trace to our logfile */
    vfprintf(ptLogFile->hFile, szFormat, vaList);
    fprintf(ptLogFile->hFile, "\n");
    fflush(ptLogFile->hFile);

  } else
  {
    printf("%.2d.%.2d.%.4d %.2d:%.2d:%.2d.%.3ld.%3ld: ",
             local_tm->tm_mday, local_tm->tm_mon + 1, local_tm->tm_year + 1900,
             local_tm->tm_hour, local_tm->tm_min, local_tm->tm_sec,
             time.tv_usec / 1000, time.tv_usec % 1000);
    /* No logfile, so print to console */
    vprintf(szFormat, vaList);
    printf("\n");
  }
}

/*****************************************************************************/
/*! Create Log-File
*     \param ptnetXTransportInst Pointer to netXTransport instance           */
/*****************************************************************************/
void USER_TraceInitialize(NETX_TRANSPORT_DATA_T* ptnetXTransportInst)
{
  int             fd;
  NXT_LOG_FILE_T* ptLogFile;

  ptLogFile = OS_Memalloc(sizeof(NXT_LOG_FILE_T));
  OS_Memset(ptLogFile, 0, sizeof(NXT_LOG_FILE_T));

  ptnetXTransportInst->pvLogFile = (void*)ptLogFile;

  /* check if log file exists */
  if (ptLogFile)
  {
    ptLogFile->hFile = NULL;
    fd = open( "netXTransport.log", O_CREAT | O_RDWR | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    if(fd > 0)
    {
      ptLogFile->hFile = fdopen(fd, "w");
    }

    if (NULL == ptLogFile->hFile)
    {
      printf("Error while creating log file. Print Debug-Messages to console!\n");
    } else
    {
      int         fd    = fileno(ptLogFile->hFile);
      int         iMode;
      struct stat buf;

      if( fstat( fd, &buf) != 0 )
      {
        perror("fstat for log-file failed!");
      } else
      {
        iMode = buf.st_mode;
        iMode |= S_IRGRP | S_IROTH;
        chmod( "netXTransport.log", iMode);
      }
    }
  }
  /* log file header */
  USER_Trace(ptnetXTransportInst, 0xFF, "----- netXTransport Log started ---------------------\n");
  USER_Trace(ptnetXTransportInst, 0xFF, "netXTransport Toolkit-Version: %d.%d.%d.%d\n", NXT_VERSION_MAJOR, NXT_VERSION_MINOR, NXT_VERSION_REV, NXT_VERSION_BUILD);
}

/*****************************************************************************/
/*! Close Log-File
*     \param ptnetXTransportInst Pointer to netXTransport instance           */
/*****************************************************************************/
void USER_TraceDeInitialize(NETX_TRANSPORT_DATA_T* ptnetXTransportInst)
{
  void* pvLogFile = ptnetXTransportInst->pvLogFile;

  if (pvLogFile)
  {
    NXT_LOG_FILE_T* ptLogFile = pvLogFile;
    if (ptLogFile->hFile)
    {
      fclose( ptLogFile->hFile);
    }
    OS_Memfree(ptLogFile);
    ptnetXTransportInst->pvLogFile = NULL;
  }
}

/*****************************************************************************/
/*! Returns connector specific timeout
*     \param ptnetXTransportInst Pointer to connector
*     \param pulTimeout          Pointer to returned timeout value           */
/*****************************************************************************/
void USER_GetConnectorTimeout( PNETX_CONNECTOR_T ptConnector, uint32_t* pulTimeout)
{
  UNREFERENCED_PARAMETER( ptConnector);

  if (pulTimeout != NULL)
    *pulTimeout = 1000;
}

/*****************************************************************************/
/*! User defined timeout thread function (starts HilTransport Timeout-Handler)
*     \param lpParameter Pointer to win32 dependent thread information structure
*     returns 0                                                              */
/*****************************************************************************/
void* UserTimeoutThread( void* lpParameter)
{
  LINUX_THREAD_PARAM_T* ptThreadParam = (LINUX_THREAD_PARAM_T*)lpParameter;

  if (NULL != ptThreadParam->pfThreadFunction)
  {
    ptThreadParam->pfThreadFunction( ptThreadParam->pvArg);
  }
  return 0;
}

/*****************************************************************************/
/*! User defined send thread function (starts HilTransport Send-Handler)
*     \param lpParameter Pointer to win32 dependent thread information structure
*     returns 0                                                              */
/*****************************************************************************/
void* UserSendThread( void* lpParameter)
{
  LINUX_THREAD_PARAM_T* ptThreadParam = (LINUX_THREAD_PARAM_T*)lpParameter;

  if (NULL != ptThreadParam->pfThreadFunction)
  {
    ptThreadParam->pfThreadFunction( ptThreadParam->pvArg);
  }
  return 0;
}

/*****************************************************************************/
/*! Function retrieves OS specific thread parameter
*     \param szThreadName     Name of thread to be started
*     \param ppvThreadParam   Returned pointer to thread parameter
*     \param pulParamSize     Size of buffer pointed by *ppvThreadParam
*     \param pfThreadFunction Pointer to returned timeout parameter info
*     return NXT_NO_ERROR on success                                         */
/*****************************************************************************/
int32_t USER_GetThreadParam( char* szThreadName, void** ppvThreadParam, uint32_t* pulParamSize, pfThreadFunc pfThreadFunction, void* pvParam)
{
  LINUX_THREAD_PARAM_T* ptThreadParam = NULL;

  if (szThreadName == NULL)
    return -1;
  else if (0 == strncmp(szThreadName, "HilTransportSendThread", strlen(szThreadName)))
  {
    ptThreadParam = (LINUX_THREAD_PARAM_T*)OS_Memalloc(sizeof(LINUX_THREAD_PARAM_T));

    memset( &ptThreadParam->tThread, 0, sizeof(ptThreadParam->tThread));
    pthread_attr_init(&ptThreadParam->tAttr);

    ptThreadParam->pfnStart         = UserSendThread;
    ptThreadParam->pvArg            = pvParam;
    ptThreadParam->pfThreadFunction = pfThreadFunction;
    ptThreadParam->pvUserParam      = ptThreadParam;

  } else if (0 == strncmp(szThreadName, "HilTransportTimeoutThread", strlen(szThreadName)))
  {
    ptThreadParam = (LINUX_THREAD_PARAM_T*)OS_Memalloc(sizeof(LINUX_THREAD_PARAM_T));

    memset( &ptThreadParam->tThread, 0, sizeof(ptThreadParam->tThread));
    pthread_attr_init(&ptThreadParam->tAttr);

    ptThreadParam->pfnStart         = UserTimeoutThread;
    ptThreadParam->pvArg            = pvParam;
    ptThreadParam->pfThreadFunction = pfThreadFunction;
    ptThreadParam->pvUserParam      = ptThreadParam;
  }

  if (ppvThreadParam)
    *ppvThreadParam = ptThreadParam;

  if (pulParamSize)
    *pulParamSize = sizeof(*ptThreadParam);

  return 0;
}

/*****************************************************************************/
/*! Function frees user thread prameter (previously allocated with with USER_GetThreadParam())
*     \param ptConnector  Pointer to connector
*     \param ptConnector  Pointer to returned timeout                        */
/*****************************************************************************/
int32_t USER_ReleaseThreadParam( void* pvThreadParam)
{
  OS_Memfree( pvThreadParam);

  return 0;
}
