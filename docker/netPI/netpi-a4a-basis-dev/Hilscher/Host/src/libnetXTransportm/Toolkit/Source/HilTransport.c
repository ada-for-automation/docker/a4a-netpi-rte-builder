/**************************************************************************************

Copyright (c) Hilscher Gesellschaft fuer Systemautomation mbH. All Rights Reserved.

***************************************************************************************

  $Id: HilTransportLayer.c $:

  Description:
    Implementation of the Hilscher Transport protocol

  Changes:
    Date        Description
    -----------------------------------------------------------------------------------
    2014-01-13  - Improved multithreading support (parallel services)
                - Improved packet handling for resource preallocation (NXT_PACKET_PREALLOCATE)
    2013-02-13  initial version

 **************************************************************************************/

/****************************************************************************/
/*! \file HilTransport.c
    Implementation of the Hilscher Transport protocol                       */
/****************************************************************************/

#include "netXTransport.h"
#include "HilTransportLayer.h"
#include "OS_Dependent.h"
#include "cifXErrors.h"
#include "OS_Includes.h"

typedef struct HIL_TRANSPORT_TRANSACTION_DATA_Ttag HIL_TRANSPORT_TRANSACTION_DATA_T, *PHIL_TRANSPORT_TRANSACTION_DATA_T /*!< Pointer to HIL_TRANSPORT_INSTANCE_T */;

/*****************************************************************************/
/*! Structure definitions if buffer pre-allocation is enabled                */
/*****************************************************************************/
#ifdef NXT_PACKET_PREALLOCATE
/*****************************************************************************/
/*! Global definitions                                                       */
/*****************************************************************************/
#define HIL_TRANSPORT_DEFAULT_PACKET_SIZE 0x1000 /* Size of pre-allocated buffers(4096 Byte) */
#define HIL_TRANSPORT_PRE_ALLOC_BUFFER    0x10   /* Number of pre-allocated buffers (16)     */

TAILQ_HEAD(HIL_TRANSPORT_PACKET_LIST, PACKET_LISTENTRY_Ttag);

typedef struct PACKET_LISTENTRY_Ttag
{
  struct ADMIN_Ttag
  {
    TAILQ_ENTRY(PACKET_LISTENTRY_Ttag) tList;           /*!< List entry                            */
    int                                fPartOfList;     /*!< marks if entry is part of packet pool */
    int                                fFree;           /*!< marks if entry is already reserved    */
    HIL_TRANSPORT_TRANSACTION_DATA_T*  ptTXTransaction; /*!< Pointer to send transaction info      */
    HIL_TRANSPORT_TRANSACTION_DATA_T*  ptRXTransaction; /*!< Pointer to recevice transaction info  */
  } tListInfo;                                          /*!< administrative structure              */
  HIL_TRANSPORT_PACKET_T             tPacket;           /*!< HIL_TRANSPORT_PACKET_T packet         */

} PACKET_LISTENTRY_T, *PPACKET_LISTENTRY_T; /*!< Management structure for pre-allocated buffer pool */
#endif

/*****************************************************************************/
/*! Global management information                                            */
/*****************************************************************************/
typedef struct HIL_TRANSPORT_INSTANCE_Ttag
{
  void*                            hNetXTransportHandle;  /*!< reference to the netXTransport instance (e.g. required for trace messages) */
  int32_t                          lRefCounter;
#ifdef NXT_PACKET_PREALLOCATE
  void*                            pvPacketListLock;
  struct HIL_TRANSPORT_PACKET_LIST tHilTransportPacketList;            /*!< List of active send transactions                      */
  PPACKET_LISTENTRY_T              ptCurTransportPacket;
#endif

} HIL_TRANSPORT_INSTANCE_T, *PHIL_TRANSPORT_INSTANCE_T /*!< Pointer to HIL_TRANSPORT_INSTANCE_T */;

/*****************************************************************************/
/*! State definitions of the recevice state machine                          */
/*****************************************************************************/
typedef enum
{
  eWAIT_FOR_COOKIE  = 0,        /*!< State machine is waiting for cookie                            */
  eWAIT_FOR_HEADER,             /*!< State machine got the cookie and is waiting to complete header */
  eWAIT_FOR_PACKET_COMPLETE,    /*!< State machine got the header and is waiting to complete packet */

} HIL_TRANSPORT_RX_STATE_E;

/*****************************************************************************/
/*! HilTransport transaction resource structure                              */
/*****************************************************************************/
struct HIL_TRANSPORT_TRANSACTION_DATA_Ttag
{
  TAILQ_ENTRY(HIL_TRANSPORT_TRANSACTION_DATA_Ttag) tList;  /*!< List entry                             */
  union
  {
    PHIL_TRANSPORT_HEADER   ptSendPacket;   /*!< Send packet                                           */
    PHIL_TRANSPORT_PACKET_T ptRecvPacket;   /*!< Receive packet (response)                             */
  } uPacket;                                /*!< Union: HilTransport packet                            */
  void*    pvComplete;                       /*!< Pointer to event, signalling transaction is completed */
  int32_t  lError;                           /*!< Error (valid if tranaction is complete)               */
  int      fPending;                         /*!< Indicates transaction is already pending              */
  int      fCancelled;                       /*!< Indicates transaction is already pending              */
#ifdef NXT_PACKET_PREALLOCATE
  int      fFree;                           /*!< Indicates resource is free                            */
#endif
#ifdef NXT_MULTITHREADED
  void*    pvRefCount;                      /*!< Number of active references                           */
  uint32_t ulTimeOut;                       /*!< Timeout required for timeout handler                  */
  int      fFreeData;                       /*!< Indicates that uPacket can be freed                   */
#endif

};

/*****************************************************************************/
/*! HilTransport Translation-Layer information structure                     */
/*****************************************************************************/
typedef struct HIL_TL_INFO_Ttag
{
  TAILQ_ENTRY(HIL_TL_INFO_Ttag)  tList;         /*!< List entry                             */
  void*                          hTLHandle;     /*!< Handle to Translation-Layer            */
  PNETX_TL_INTERFACE_T           ptTLInterface; /*!< Pointer to Translation-Layer interface */

} HIL_TL_INFO_T, *PHIL_TL_INFO_T /*!< Pointer to HIL_TL_INFO_T */;

TAILQ_HEAD(HIL_TRANSACTIONS_T, HIL_TRANSPORT_TRANSACTION_DATA_Ttag);
TAILQ_HEAD(HIL_TL_INFO_LIST_T, HIL_TL_INFO_Ttag);

/*****************************************************************************/
/*! HilTransport Keep-Alive information structure                            */
/*****************************************************************************/
typedef struct TRANSPORT_KEEPALIVE_Ttag
{
  KEEPALIVE_STATE_E eKeepAliveState;     /*!< State of Keep-Alive handling            */
  uint32_t          ulKeepAliveIdent;    /*!< Ident number of Keep-Alive handling     */
  uint32_t          ulKeepAliveTimeout;  /*!< Connection dependent Keep-Alive timeout */
  uint32_t          ulLastKeepAlive;     /*!< Timestamp of last transmission          */

} TRANSPORT_KEEPALIVE_T;

/*****************************************************************************/
/*! HilTransport thread information structure                                */
/*****************************************************************************/
typedef struct THREAD_RESOURCES_Ttag
{
  void*           pvThreadParam;        /*!< Pointer to OS dependent thread information   */
  uint32_t        ulParamSize;          /*!< Size of information pointed by pvThreadParam */

} THREAD_RESOURCES_T;

/*****************************************************************************/
/*! HilTransport instance information structure                              */
/*****************************************************************************/
typedef struct HIL_TRANSPORT_DATA_Ttag
{
  TAILQ_ENTRY(HIL_TRANSPORT_DATA_Ttag) tList;                      /*!< List entry                                            */
  struct HIL_TL_INFO_LIST_T            tTLInfoList;                /*!< List of supported Translation-Layers                  */
  PNETX_CONNECTOR_T                    ptConnector;                /*!< Pointer to netXTransport connector                    */
  PHIL_TL_INFO_T                       ptDefaultTranslationLayer;  /*!< Default Translation-Layer (deliver incoming requests) */
  uint8_t                              bLastSequenceNr;            /*!< Last sequence number                                  */
  uint16_t                             usLastTransaction;          /*!< Last transaction number                               */
  char                                 szInterfaceName[16];        /*!< Interface name                                        */
  void*                                pvConnectorData;            /*!< Private connector data                                */
  void*                                pvConnectorInterface;       /*!<                                                       */
  TRANSPORT_SERVER_INFO_T              tServerInfo;                /*!< Server information                                    */
  HIL_TRANSPORT_RX_STATE_E             eRxState;                   /*!< State of the receive state machine                    */
  HIL_TRANSPORT_HEADER                 tRxCurrentHeader;           /*!< Current receive header                                */
  uint32_t                             ulRXCurrentOffset;          /*!< Current send header                                   */
  PHIL_TRANSPORT_PACKET_T              ptCurrentRxPacket;          /*!< Current receive packet                                */
  uint32_t                             ulDataOffset;               /*!< Current data offset                                   */
  void*                                hTXTransactionsLock;        /*!< Pointer to send transaction lock                      */
  struct HIL_TRANSACTIONS_T            tTXTransactions;            /*!< List of processed send transactions                   */
  void*                                hRXTransactionsLock;        /*!< Pointer to receive transaction lock                   */
  struct HIL_TRANSACTIONS_T            tRXTransactions;            /*!< List of active receive transactions                   */
  TRANSPORT_KEEPALIVE_T                tKeepAlive;                 /*!< State of Keep-Alive handling                          */
  uint32_t                             ulActiveSends;              /*!< Number of active sends (->tSendQueue)                 */
#ifdef NXT_MULTITHREADED
  struct HIL_TRANSACTIONS_T            tSendQueue;                 /*!< List of queued send transactions                      */
  void*                                pvSendQueueLock;            /*!< Pointer to tSendQueue lock                            */
  void*                                pvActiveSends;              /*!< Number of currently active sends                      */
  THREAD_RESOURCES_T                   tSendThreadInfo;            /*!< Pointer to send thread information                    */
  int                                  fRunSndThread;              /*!< Condition flag for send thread                        */
  void*                                pvSendDataSemaphore;        /*!< Pointer to send semaphore                             */
  void*                                pvSendEvent;                /*!< Pointer to to event signalled when number of active   */
                                                                   /*!< sends goes below watermark (parallel services)        */
  THREAD_RESOURCES_T                   tTimeoutThreadInfo;         /*!< Pointer to send thread information                    */
  int                                  fRunTimeoutThread;          /*!< Condition flag for timeout thread                     */
#else
  void*                                pvSendLock;
#endif
#ifdef NXT_HILTRANSPORT_DEBUG
  uint32_t                             ulMaxParallelSends;
  uint32_t                             ulSendWaitCount;
  uint32_t                             ulSendWaitCountMax;
#endif

} HIL_TRANSPORT_DATA_T, *PHIL_TRANSPORT_DATA_T /*!< Pointer to HIL_TRANSPORT_DATA_T */;

/* local functions */
static void     HilTransportRxData          ( uint8_t*              pabRxBuffer, uint32_t                                 ulReadLen, void* pvUser);
static int32_t  HilTransportQueryServerData ( PHIL_TRANSPORT_DATA_T ptTransport, PHIL_TRANSPORT_ADMIN_QUERYSERVER_DATA_T* pptServerInfo);
static uint16_t CalcCrc16                   ( uint8_t*              pbData,      uint32_t                                 ulDataLen);
static uint16_t CalcCrcNetIC                ( uint8_t*              pbData,      uint32_t                                 ulDataLen);
static int32_t  cifXErrorFromTransportState ( uint8_t               bState);
#ifdef NXT_MULTITHREADED
static void     HilTransportSendThread      ( void*                 pvParam);
static void     HilTransportTimeoutThread   ( void*                 pvParam);
static void     HilTransportCtlSendThread   ( PHIL_TRANSPORT_DATA_T ptTransport, int fRun);
static void     HilTransportCtlTimeoutThread( PHIL_TRANSPORT_DATA_T ptTransport, int fRun);
#endif
static int32_t  HilTransportSendPacketEx    ( NXTHANDLE hTransport, HIL_TRANSPORT_HEADER* ptPacket, uint32_t ulConTimeout, uint32_t ulTimeout, int fCompleteTransfer);

#ifdef NXT_PACKET_PREALLOCATE
  static PPACKET_LISTENTRY_T HilTransportAllocateListPacket(NXTHANDLE hTransport, uint32_t ulDataSize, uint16_t usDataType);
#endif
static HIL_TRANSPORT_TRANSACTION_DATA_T* HilTransportAllocateTXTransaction( PHIL_TRANSPORT_DATA_T ptTransport, PHIL_TRANSPORT_HEADER ptPacket);
static HIL_TRANSPORT_TRANSACTION_DATA_T* HilTransportAllocateRXTransaction( PHIL_TRANSPORT_DATA_T ptTransport, PHIL_TRANSPORT_HEADER ptPacket);
static void                              HilTransportFreeTransaction      ( HIL_TRANSPORT_TRANSACTION_DATA_T* ptTransaction);


static HIL_TRANSPORT_INSTANCE_T s_tHilTransportInst = {0};

/*****************************************************************************/
/*! Global preparation for Transport-Layer. Needs to be called once before running any
*   other HilTransportXX() function!
*   \param hnetXTransport  Handle to the global netXTransport Instance
*   \return NXT_NO_ERROR on success                                          */
/*****************************************************************************/
int32_t HilTransportPrepare( void* hnetXTransport)
{
#ifdef NXT_PACKET_PREALLOCATE /* preallocate send/receive resources */
  uint32_t            ulBufferCounter = 0;
  PPACKET_LISTENTRY_T ptPacketEntry   = NULL;

  if (NULL != (s_tHilTransportInst.pvPacketListLock = OS_CreateLock()))
  {
    OS_EnterLock( s_tHilTransportInst.pvPacketListLock);

    /* pre-allocate buffers */
    TAILQ_INIT( &s_tHilTransportInst.tHilTransportPacketList);

    while((NULL != (ptPacketEntry = HilTransportAllocateListPacket( NULL, HIL_TRANSPORT_DEFAULT_PACKET_SIZE, 0))) && (ulBufferCounter < HIL_TRANSPORT_PRE_ALLOC_BUFFER))
    {
      TAILQ_INSERT_TAIL( &s_tHilTransportInst.tHilTransportPacketList, ptPacketEntry, tListInfo.tList);
      ptPacketEntry->tListInfo.fPartOfList = 1;
      ulBufferCounter++;
    }
    if (ulBufferCounter != HIL_TRANSPORT_PRE_ALLOC_BUFFER)
    {
      USER_Trace( hnetXTransport, TRACE_LEVEL_ERROR, "Failed to pre-allocate requested send buffer (%d/%d)", ulBufferCounter, HIL_TRANSPORT_PRE_ALLOC_BUFFER);
    }
    OS_LeaveLock( s_tHilTransportInst.pvPacketListLock);
  }
#endif
  s_tHilTransportInst.hNetXTransportHandle = hnetXTransport; /* the handle is required to be able to use User_Trace() */

  return NXT_NO_ERROR;
}

/*****************************************************************************/
/*! Global de-initialization Transport-Layer. Frees all global allocated resources
*   (see HilTransportPrepare())!                                             */
/*****************************************************************************/
void HilTransportFreeResources( void)
{
#ifdef NXT_PACKET_PREALLOCATE
  PPACKET_LISTENTRY_T ptPacketEntry = NULL;

  if (s_tHilTransportInst.pvPacketListLock != NULL)
  {
    OS_EnterLock( s_tHilTransportInst.pvPacketListLock);

    while(NULL != (ptPacketEntry = TAILQ_FIRST( &s_tHilTransportInst.tHilTransportPacketList)))
    {
      TAILQ_REMOVE( &s_tHilTransportInst.tHilTransportPacketList, ptPacketEntry, tListInfo.tList);
      /* send transaction resources */
      if (NULL != ptPacketEntry->tListInfo.ptTXTransaction)
      {
        if (NULL != ptPacketEntry->tListInfo.ptTXTransaction->pvComplete)
        {
          OS_DeleteEvent( ptPacketEntry->tListInfo.ptTXTransaction->pvComplete);
        }
#ifdef NXT_MULTITHREADED
        if (NULL != ptPacketEntry->tListInfo.ptTXTransaction->pvRefCount)
        {
          OS_DeleteInterLockedVariable( ptPacketEntry->tListInfo.ptTXTransaction->pvRefCount);
        }
#endif
        OS_Memfree(ptPacketEntry->tListInfo.ptTXTransaction);
      }
      /* receive transaction resources */
      if (NULL != ptPacketEntry->tListInfo.ptRXTransaction)
      {
        if (NULL != ptPacketEntry->tListInfo.ptRXTransaction->pvComplete)
        {
          OS_DeleteEvent( ptPacketEntry->tListInfo.ptRXTransaction->pvComplete);
        }
#ifdef NXT_MULTITHREADED
        if (NULL != ptPacketEntry->tListInfo.ptRXTransaction->pvRefCount)
        {
          OS_DeleteInterLockedVariable( ptPacketEntry->tListInfo.ptRXTransaction->pvRefCount);
        }
#endif
        OS_Memfree(ptPacketEntry->tListInfo.ptRXTransaction);
      }
      OS_Memfree( ptPacketEntry);
    }

    OS_LeaveLock( s_tHilTransportInst.pvPacketListLock);
  }
  OS_DeleteLock( s_tHilTransportInst.pvPacketListLock);

  s_tHilTransportInst.pvPacketListLock = NULL;
#endif
}

/*****************************************************************************/
/*! Initializes Transport Layer for specific connector.
*   \param ptConnector      Pointer to the connector instance
*   \param szInterfaceName  Name of the interface which should be initialized
*   \param pvConnectorData  Private connector data
*   \param ausSupportedTL   Array of available translation layers
*   \param phTransport      Handle to initialized instance of the transport layer
*   \param pfnDevNotify     Called a device is detected and its translation layer is successfully initialized
*   \return NXT_NO_ERROR on success                                          */
/*****************************************************************************/
int32_t HilTransportInit( PNETX_CONNECTOR_T ptConnector, const char* szInterfaceName, void* pvConnectorData, uint16_t* ausSupportedTL, NXTHANDLE* phTransport, PFN_NETXCON_DEVICE_NOTIFY_CALLBACK pfnDevNotify)
{
  PHIL_TRANSPORT_DATA_T ptTransport = OS_Memalloc(sizeof(HIL_TRANSPORT_DATA_T));
  int32_t               lRet        = NXT_OUT_OF_MEMORY;

  if(NULL != ptTransport)
  {
    OS_Memset(ptTransport, 0, sizeof(*ptTransport));

    TAILQ_INIT(&ptTransport->tRXTransactions);
    ptTransport->hRXTransactionsLock = OS_CreateLock();

    TAILQ_INIT(&ptTransport->tTXTransactions);
    ptTransport->hTXTransactionsLock = OS_CreateLock();

    TAILQ_INIT(&ptTransport->tTLInfoList);

#ifndef NXT_MULTITHREADED /* lock required to synchronize send resources */
    ptTransport->pvSendLock = OS_CreateLock();
#endif

    ptTransport->ptConnector      = ptConnector;
    ptTransport->pvConnectorData  = pvConnectorData;
    OS_Strncpy(ptTransport->szInterfaceName, szInterfaceName, 16);

    /* Try to open the interface */
    if(NULL != (ptTransport->pvConnectorInterface = ptConnector->tFunctions.pfnConCreateInterface(szInterfaceName)))
    {
      /* Try to start interface */
      if(NXT_NO_ERROR == (lRet = ptConnector->tFunctions.pfnConIntfStart(ptTransport->pvConnectorInterface, HilTransportRxData, ptTransport)))
      {
        int32_t         lIdx = 0;
        uint16_t const* ausTL;
        uint32_t        ulTLCount;

#ifdef NXT_MULTITHREADED
        /* setup at least one service to be able to send data */
        ptTransport->tServerInfo.ulParallelServices = 1;
        ptTransport->pvSendEvent                    = OS_CreateEvent();
        ptTransport->pvSendDataSemaphore            = OS_CreateSemaphore(0);
        ptTransport->pvActiveSends                  = OS_CreateInterLockedVariable();

        TAILQ_INIT(&ptTransport->tSendQueue);
        ptTransport->pvSendQueueLock = OS_CreateLock();

        /* get user parameter and initialize required parameter for OS-specific thread creation */
        USER_GetThreadParam( "HilTransportTimeoutThread",
                              &ptTransport->tTimeoutThreadInfo.pvThreadParam,
                              &ptTransport->tTimeoutThreadInfo.ulParamSize,
                              HilTransportTimeoutThread,
                              (void*)ptTransport);

        HilTransportCtlTimeoutThread( ptTransport, 1);

        /* create send receive thread */
        OS_CreateThread( ptTransport->tTimeoutThreadInfo.pvThreadParam, ptTransport->tTimeoutThreadInfo.ulParamSize);

        USER_GetThreadParam( "HilTransportSendThread",
                              &ptTransport->tSendThreadInfo.pvThreadParam,
                              &ptTransport->tSendThreadInfo.ulParamSize,
                              HilTransportSendThread,
                              (void*)ptTransport);

        HilTransportCtlSendThread( ptTransport, 1);

        /* create send receive thread */
        OS_CreateThread( ptTransport->tSendThreadInfo.pvThreadParam, ptTransport->tSendThreadInfo.ulParamSize);

        /* wait for thread to be started  */
        //OS_Sleep(1000);
#endif

        if (NXT_NO_ERROR == (lRet = HilTransportQueryServerData( ptTransport, &ptTransport->tServerInfo.ptQueryServerData)))
        {
          ausTL     = ptTransport->tServerInfo.ptQueryServerData->ausDataTypes;
          ulTLCount = ptTransport->tServerInfo.ptQueryServerData->ulDatatypeCnt;

          ptTransport->tServerInfo.fIgnoreSeqNr        = 0;
          ptTransport->tServerInfo.ulBufferSize        = ptTransport->tServerInfo.ptQueryServerData->ulBufferSize;
          ptTransport->tServerInfo.ulFeatures          = ptTransport->tServerInfo.ptQueryServerData->ulFeatures;
          ptTransport->tServerInfo.ulParallelServices  = ptTransport->tServerInfo.ptQueryServerData->ulParallelServices;

          /* validate feature flags */
          if ((ptTransport->tServerInfo.ulFeatures & HIL_TRANSPORT_FEATURES_KEEPALIVE))
          {
            ptTransport->tKeepAlive.eKeepAliveState = eKEEP_ALIVE_INITIALIZATION;
          }

        } else
        {
          /* No Query server features, so we need to probe every available translation layer */
          ptTransport->tServerInfo.ulBufferSize       = 6000;
          ptTransport->tServerInfo.ulParallelServices = 1;
          ptTransport->tServerInfo.ulFeatures         = 0;
          ptTransport->tServerInfo.fIgnoreSeqNr       = 1;

          ausTL     = ausSupportedTL;
          ulTLCount = 0;
          if (NULL != ausSupportedTL)
          {
            while(ausSupportedTL[ulTLCount] != 0x0000) /* 0x0000 marks end of array */
            {
              ulTLCount++;
            }
          }
        }
        ptTransport->ptDefaultTranslationLayer = NULL;

        for(lIdx = ulTLCount - 1; lIdx >= 0; lIdx--)
        {
          PNETX_TL_INTERFACE_T ptTLInterface = netXTransportGetTranslationLayer(ausTL[lIdx]);
          NXTHANDLE            hTLHandle;

          if(ptTLInterface != NULL)
          {
            uint32_t ulDeviceCount = 0;

            /* check if server supports current translation layer */
            if(NXT_NO_ERROR == (lRet = ptTLInterface->pfnProbe(ptTransport, &hTLHandle, &ulDeviceCount, &ptTransport->tServerInfo)))
            {
              PHIL_TL_INFO_T ptLayerInstance = OS_Memalloc(sizeof(*ptLayerInstance));

              if(NULL == ptLayerInstance)
              {
                /* Out of memory */
                if(g_ulTraceLevel & TRACE_LEVEL_ERROR)
                {
                  USER_Trace( s_tHilTransportInst.hNetXTransportHandle, TRACE_LEVEL_ERROR, "Error allocating memory for Receive-Packet (Packet-ID:%d)!\n", ptTransport->tRxCurrentHeader.bSequenceNr);
                }
                ptTLInterface->pfnRelease( hTLHandle);
              } else
              {
                void*    pvDeviceData       = NULL;
                uint32_t ulDevice           = 0;
                uint32_t ulDeviceIdentifier = 0;
                char     szDeviceName[NXT_MAX_PATH];

                OS_Memset(ptLayerInstance, 0, sizeof(*ptLayerInstance));
                ptLayerInstance->ptTLInterface  = ptTLInterface;
                ptLayerInstance->hTLHandle      = hTLHandle;
                TAILQ_INSERT_TAIL(&ptTransport->tTLInfoList, ptLayerInstance, tList);

                /* TODO: do not set default layer here */
                /* TODO: if we set it in the driver open method it is possible to switch between different data types */
                ptTransport->ptDefaultTranslationLayer = ptLayerInstance;

                /* scan for all available boards end initialize the data typical resources needed for every device */
                while((ulDevice < ulDeviceCount) && (NXT_NO_ERROR == ptTLInterface->pfnScan( hTLHandle, szDeviceName, &ulDeviceIdentifier, &pvDeviceData, ulDevice)))
                {
                  /* notify a new device, to the global management structure in the netXTransport Layer */
                  pfnDevNotify( szInterfaceName, szDeviceName, ulDeviceIdentifier, eNXT_DEVICE_ATTACHED, (void*)ptTransport, pvDeviceData);
                  ulDevice++;
                  pvDeviceData = NULL;
                }
                break;
              }
            }
          }
        }

        if(NULL == ptTransport->ptDefaultTranslationLayer)
        {
          /* we failed to establish a connection to a server */
          ptConnector->tFunctions.pfnConIntfStop( ptTransport->pvConnectorInterface);

          /* No translation layer to communicate, so this must be an unsupported device */
          lRet = NXT_UNSUPPORTED_DEVICE;
        } else
        {
#ifdef NXT_DISABLE_KEEPALIVE
          if(ptTransport->tKeepAlive.eKeepAliveState != eKEEP_ALIVE_UNSUPPORTED)
          {
            USER_Trace( s_tHilTransportInst.hNetXTransportHandle,
                        TRACE_LEVEL_DEBUG,
                        "Disabling Keep-Alive for interface %s\n",
                        szInterfaceName);

            ptTransport->tKeepAlive.eKeepAliveState = eKEEP_ALIVE_UNSUPPORTED;
          }
#endif
          *phTransport = ptTransport;
          s_tHilTransportInst.lRefCounter++;
        }
      }
    }
    /* in case of an error free ptTransport */
    if (lRet != NXT_NO_ERROR && ptTransport != NULL)
    {
#ifdef NXT_MULTITHREADED
      HilTransportCtlTimeoutThread( ptTransport, 0);
      if (NULL != ptTransport->tTimeoutThreadInfo.pvThreadParam)
      {
        OS_DeleteThread(ptTransport->tTimeoutThreadInfo.pvThreadParam);
        USER_ReleaseThreadParam( ptTransport->tTimeoutThreadInfo.pvThreadParam);
      }
      HilTransportCtlSendThread( ptTransport, 0);
      if (NULL != ptTransport->tTimeoutThreadInfo.pvThreadParam)
      {
        OS_DeleteThread(ptTransport->tSendThreadInfo.pvThreadParam);
        USER_ReleaseThreadParam( ptTransport->tSendThreadInfo.pvThreadParam);
      }
      if (NULL != ptTransport->pvSendEvent)
        OS_DeleteEvent( ptTransport->pvSendEvent);
      if (NULL != ptTransport->pvSendDataSemaphore)
        OS_DeleteSemaphore( ptTransport->pvSendDataSemaphore);
      if (NULL != ptTransport->pvSendQueueLock)
        OS_DeleteLock( ptTransport->pvSendQueueLock);
if (ptTransport->pvActiveSends)
        OS_DeleteInterLockedVariable( ptTransport->pvActiveSends);
#else
      if (NULL != ptTransport->pvSendLock)
        OS_DeleteLock( ptTransport->pvSendLock);
#endif
      OS_DeleteLock( ptTransport->hRXTransactionsLock);
      OS_DeleteLock( ptTransport->hTXTransactionsLock);

      OS_Memfree(ptTransport);
    }
  }
  return lRet;
}

/*****************************************************************************/
/*! De-initializes Transport Layer.
*   \param hTransport  Handle to transport layer instance                    */
/*****************************************************************************/
void HilTransportDeinit( NXTHANDLE hTransport)
{
  PHIL_TRANSPORT_DATA_T ptTransport = (PHIL_TRANSPORT_DATA_T)hTransport;
  PHIL_TL_INFO_T        ptLayerInstance;

  /* Stop the interface */
  ptTransport->ptConnector->tFunctions.pfnConIntfStop(ptTransport->pvConnectorInterface);

#ifndef NXT_MULTITHREADED
  HilTransportStopPendingTransactions( hTransport, 1);
#else
  HilTransportCtlSendThread( ptTransport, 0);
  OS_DeleteThread( ptTransport->tSendThreadInfo.pvThreadParam);
  HilTransportCtlTimeoutThread( ptTransport, 0);
  OS_DeleteThread( ptTransport->tTimeoutThreadInfo.pvThreadParam);

  USER_ReleaseThreadParam( ptTransport->tTimeoutThreadInfo.pvThreadParam);
  USER_ReleaseThreadParam( ptTransport->tSendThreadInfo.pvThreadParam);

   HilTransportStopPendingTransactions( hTransport, 1);

  OS_DeleteEvent( ptTransport->pvSendEvent);
  OS_DeleteSemaphore( ptTransport->pvSendDataSemaphore);
  OS_DeleteLock( ptTransport->pvSendQueueLock);
  OS_DeleteInterLockedVariable( ptTransport->pvActiveSends);
#endif

  /* Deinit and delete all translation layer */
  while(NULL != (ptLayerInstance = TAILQ_FIRST(&ptTransport->tTLInfoList)))
  {
    /* Call deinit on translation layer */
    PNETX_TL_INTERFACE_T ptTLInterface = ptLayerInstance->ptTLInterface;

    ptTLInterface->pfnRelease( ptLayerInstance->hTLHandle);

    TAILQ_REMOVE(&ptTransport->tTLInfoList, ptLayerInstance, tList);
    OS_Memfree(ptLayerInstance);
  }
#ifndef NXT_MULTITHREADED
    OS_DeleteLock( ptTransport->pvSendLock);
#endif

  if(NULL != ptTransport->hRXTransactionsLock)
  {
    OS_DeleteLock(ptTransport->hRXTransactionsLock);
    ptTransport->hRXTransactionsLock = NULL;
  }

  if(NULL != ptTransport->hTXTransactionsLock)
  {
    OS_DeleteLock(ptTransport->hTXTransactionsLock);
    ptTransport->hTXTransactionsLock = NULL;
  }

  if(NULL != ptTransport->tServerInfo.ptQueryServerData)
  {
    OS_Memfree(ptTransport->tServerInfo.ptQueryServerData);
    ptTransport->tServerInfo.ptQueryServerData = NULL;
  }

  OS_Memfree(ptTransport);
}

/*****************************************************************************/
/*! HiltTransport initialization for specific connector, in case of a reconnect.
    (Called in case of netXTransport-Server based endpoint)
*   \param hTransport          Handle to transport layer instance
*   \param ulDeviceIdentifier  Unique identifier of the device which should be reconnected
*   \param pfnDevNotify        Called everytime a device is detected and its translation layer is successfully initialized
*   \return NXT_NO_ERROR on success                                          */
/*****************************************************************************/
int32_t HilTransportSoftReconnect( NXTHANDLE hTransport, uint32_t ulDeviceIdentifier, PFN_NETXCON_DEVICE_NOTIFY_CALLBACK pfnDevNotify)
{
  PHIL_TRANSPORT_DATA_T ptTransport = (PHIL_TRANSPORT_DATA_T)hTransport;
  int32_t               lRet        = NXT_OUT_OF_MEMORY;

  if(NULL != ptTransport)
  {
    PHIL_TL_INFO_T ptLayerInstance = ptTransport->ptDefaultTranslationLayer;

    if (ptLayerInstance != NULL)
    {
      PNETX_TL_INTERFACE_T ptTLInterface = ptLayerInstance->ptTLInterface;

      if(ptTLInterface != NULL)
      {
        uint32_t  ulDeviceCount = 0;
        NXTHANDLE hTmpTLHandle  = NULL;

        /* check if server supports current translation layer */
        if(NXT_NO_ERROR == (lRet = ptTLInterface->pfnProbe( ptTransport, &hTmpTLHandle, &ulDeviceCount, &ptTransport->tServerInfo)))
        {
          void*    pvDeviceData  = NULL;
          uint32_t ulDevice      = 0;
          uint32_t ulDeviceIdent = 0;
          char     szDeviceName[NXT_MAX_PATH];

          /* scan for all available boards end initialize the data typical resources needed for every device */
          while((ulDevice < ulDeviceCount) && (NXT_NO_ERROR == ptTLInterface->pfnScan( ptLayerInstance->hTLHandle, szDeviceName, &ulDeviceIdent, &pvDeviceData, ulDevice)))
          {
            if (ulDeviceIdentifier == ulDeviceIdent)
            {
              /* notify a new device, to the global management structure in the netXTransport Layer */
              pfnDevNotify( ptTransport->szInterfaceName, szDeviceName, ulDeviceIdentifier, eNXT_DEVICE_ATTACHED, (void*)ptTransport, pvDeviceData);
              break;
            }
            ulDevice++;
            pvDeviceData = NULL;
          }
        }
      }
    }
  }
  return lRet;
}

/*****************************************************************************/
/*! Restores the connection states (open active connections and update internal resources).
*   \param hnetXTransportDevHandle  Hanlde to netXTransport device
*   \param hTransport               Handle to the Transport Layer            */
/*****************************************************************************/
void HilTransportRestoreConnectionStates ( NXTHANDLE hnetXTransportDevHandle, NXTHANDLE hTransport)
{
  PHIL_TRANSPORT_DATA_T ptTransport     = hTransport;
  PHIL_TL_INFO_T        ptLayerInstance = ptTransport->ptDefaultTranslationLayer;

  ptLayerInstance->ptTLInterface->pfnRestoreCon( hnetXTransportDevHandle);
}

/*****************************************************************************/
/*! De-queues remaining transactions of the transport layer instance pointed by hTransport.
*   \param hTransport  Handle of transport layer                             */
/*****************************************************************************/
void HilTransportStopPendingTransactions(NXTHANDLE hTransport, int fIgnoreRemainingRequests)
{
  PHIL_TRANSPORT_DATA_T             ptTransport        = (PHIL_TRANSPORT_DATA_T)hTransport;
  PHIL_TRANSPORT_TRANSACTION_DATA_T ptTransaction      = NULL;
  int                               fRequestsRemaining = 1;

#ifdef NXT_MULTITHREADED
  /* clear all pending transactions */
  OS_EnterLock(ptTransport->pvSendQueueLock);
  do
  {
    ptTransaction = TAILQ_FIRST( &ptTransport->tSendQueue);
    fRequestsRemaining = 0;
    while (ptTransaction)
    {
      PHIL_TRANSPORT_TRANSACTION_DATA_T ptNext = TAILQ_NEXT(ptTransaction, tList);

      if (OS_InterLockedRead( ptTransaction->pvRefCount) == 0)
      {
        PHIL_TRANSPORT_PACKET_T ptTmp = (LONG2PTR(PTR2LONG(ptTransaction->uPacket.ptSendPacket) - sizeof(HIL_TRANSPORT_PACKET_T)));

        TAILQ_REMOVE(&ptTransport->tSendQueue, ptTransaction, tList);
        HilTransportFreePacket( ptTmp);

        HilTransportFreeTransaction( ptTransaction);

      } else
      {
        if (fIgnoreRemainingRequests == 0)
          fRequestsRemaining = 1;

        ptTransaction->lError = CIFX_TRANSPORT_ABORTED;
        OS_SetEvent( ptTransaction->pvComplete);
      }
      ptTransaction = ptNext;
    }
    if (fRequestsRemaining)
      OS_Sleep(1);

  }while (fRequestsRemaining);
  OS_LeaveLock(ptTransport->pvSendQueueLock);
#else
  UNREFERENCED_PARAMETER(fIgnoreRemainingRequests);
#endif
  /* clear all pending transactions */
  do
  {
    OS_EnterLock(ptTransport->hTXTransactionsLock);
    ptTransaction = TAILQ_FIRST( &ptTransport->tTXTransactions);
    fRequestsRemaining = 0;
    while (ptTransaction)
    {
      PHIL_TRANSPORT_TRANSACTION_DATA_T ptNxtTransaction = TAILQ_NEXT(ptTransaction, tList);

      if (ptTransaction->fPending == 0)
      {
#ifdef NXT_MULTITHREADED
        /* check if sombody refers to the transaction */
        if (OS_InterLockedRead( ptTransaction->pvRefCount) == 0)
        {
          PHIL_TRANSPORT_PACKET_T ptTmp = (LONG2PTR(PTR2LONG(ptTransaction->uPacket.ptSendPacket) - sizeof(HIL_TRANSPORT_PACKET_T)));

          ptTransaction->fPending = 0;

          /* remove from queue */
          TAILQ_REMOVE(&ptTransport->tTXTransactions, ptTransaction, tList);

          /* check if we need to free the user data */
          if (ptTransaction->fFreeData == 1)
            HilTransportFreePacket( ptTmp);

          HilTransportFreeTransaction( ptTransaction);
        } else
        {
          /* ignore not finished requests */
          if (fIgnoreRemainingRequests == 0)
            fRequestsRemaining = 1;

          /* signal request stopped */
          ptTransaction->lError = CIFX_TRANSPORT_ABORTED;
          OS_SetEvent( ptTransaction->pvComplete);
        }
#else
        TAILQ_REMOVE( &ptTransport->tTXTransactions, ptTransaction, tList);
        ptTransaction->fPending = 0;
        ptTransaction->lError   = CIFX_TRANSPORT_ABORTED;
        OS_SetEvent(ptTransaction->pvComplete);
#endif
      }
      ptTransaction = ptNxtTransaction;
    }
    OS_LeaveLock(ptTransport->hTXTransactionsLock);
    if (fRequestsRemaining)
      OS_Sleep(1);

  }while(fRequestsRemaining);

  OS_EnterLock(ptTransport->hRXTransactionsLock);
  TAILQ_FOREACH(ptTransaction, &ptTransport->tRXTransactions, tList)
  {
    if (ptTransaction->fPending)
    {
      TAILQ_REMOVE(&ptTransport->tRXTransactions, ptTransaction, tList);
      ptTransaction->fPending   = 0;
      ptTransaction->lError     = CIFX_TRANSPORT_ABORTED;
      ptTransaction->fCancelled = 1;
      OS_SetEvent(ptTransaction->pvComplete);
    }
  }
  OS_LeaveLock(ptTransport->hRXTransactionsLock);
}

/*****************************************************************************/
/*! Returns transport layer features
*   \param hTransport  Handle to transport layer instance
*   \return Bit field of supported features (HIL_TRANSPORT_FEATURES_*)       */
/*****************************************************************************/
uint32_t HilTransportGetFeatures( NXTHANDLE hTransport)
{
  PHIL_TRANSPORT_DATA_T ptTransport = (PHIL_TRANSPORT_DATA_T)hTransport;
  return ptTransport->tServerInfo.ulFeatures;
}

/*****************************************************************************/
/*! Returns handle of the currently used default translation layer.
*   \param hTransport  Handle to transport layer instance
*   \return NULL on failure                                                  */
/*****************************************************************************/
NXTHANDLE HilTransportGetTLHandle( NXTHANDLE hTransport)
{
  PHIL_TRANSPORT_DATA_T ptTransport = (PHIL_TRANSPORT_DATA_T)hTransport;

  if ((ptTransport != NULL) && (ptTransport->ptDefaultTranslationLayer != NULL))
    return ptTransport->ptDefaultTranslationLayer->hTLHandle;
  else
    return NULL;
}

/*****************************************************************************/
/*! Retrieves data type of the currently used default translation layer.
*   \param hTransport  Handle to transport layer instance
*   \return Data Type (e.g. 0x100, 0x200, ...)                               */
/*****************************************************************************/
uint16_t HilTransportGetDefaultTLType( NXTHANDLE hTransport)
{
  PHIL_TRANSPORT_DATA_T ptTransport = (PHIL_TRANSPORT_DATA_T)hTransport;

  return ptTransport->ptDefaultTranslationLayer->ptTLInterface->usDataType;
}

#ifdef NXT_PACKET_PREALLOCATE
/*****************************************************************************/
/*! Allocates and initializes a List-Packet.
*   \param hTransport  Handle to transport layer instance
*   \param ulDataSize   Size of data section
*   \param usDataType   Type of packet (according to the used translation layer)
*   \return Pointer to the allocated packet                                  */
/*****************************************************************************/
static PPACKET_LISTENTRY_T HilTransportAllocateListPacket(NXTHANDLE hTransport, uint32_t ulDataSize, uint16_t usDataType)
{
  PHIL_TRANSPORT_PACKET_T ptPacket          = NULL;
  PPACKET_LISTENTRY_T     ptListEntry       = OS_Memalloc(sizeof(PACKET_LISTENTRY_T) + sizeof(*ptPacket) + sizeof(*ptPacket->ptHeader) + ulDataSize);
  int                     fAllocationFailed = 0;

  if(ptListEntry)
  {
    ptPacket = &ptListEntry->tPacket;

    OS_Memset(ptPacket, 0, sizeof(*ptPacket));
    ptPacket->hTransport = hTransport;

    ptPacket->ptHeader   = (PHIL_TRANSPORT_HEADER)(ptPacket + 1);
    OS_Memset(ptPacket->ptHeader, 0, sizeof(*ptPacket->ptHeader));

    ptPacket->pbData               = (uint8_t*)(ptPacket->ptHeader + 1);
    ptPacket->ulMaxSize            = ulDataSize;
    ptPacket->ptHeader->ulCookie   = HIL_TRANSPORT_COOKIE;
    ptPacket->ptHeader->usDataType = usDataType;
    ptPacket->ptHeader->usChecksum = 0; /* we don't have a checksum yet */

    ptListEntry->tListInfo.fFree       = 1;
    ptListEntry->tListInfo.fPartOfList = 0;

    /* allocate send transaction resource */
    if (NULL != (ptListEntry->tListInfo.ptTXTransaction = OS_Memalloc(sizeof(HIL_TRANSPORT_TRANSACTION_DATA_T))))
    {
      ptListEntry->tListInfo.ptTXTransaction->fFree = 1;
      if (NULL == (ptListEntry->tListInfo.ptTXTransaction->pvComplete = OS_CreateEvent()))
        fAllocationFailed = 1;
#ifdef NXT_MULTITHREADED
      if (NULL == (ptListEntry->tListInfo.ptTXTransaction->pvRefCount = OS_CreateInterLockedVariable()))
        fAllocationFailed = 1;
#endif
    }
    /* allocate receive transaction resource */
    if (NULL != (ptListEntry->tListInfo.ptRXTransaction = OS_Memalloc(sizeof(HIL_TRANSPORT_TRANSACTION_DATA_T))))
    {
      ptListEntry->tListInfo.ptRXTransaction->fFree = 1;
      if (NULL == (ptListEntry->tListInfo.ptRXTransaction->pvComplete = OS_CreateEvent()))
        fAllocationFailed = 1;
#ifdef NXT_MULTITHREADED
      if (NULL == (ptListEntry->tListInfo.ptRXTransaction->pvRefCount = OS_CreateInterLockedVariable()))
        fAllocationFailed = 1;
#endif
    }
    /* allocation failed so free resources */
    if (fAllocationFailed == 1)
    {
      if (ptListEntry->tListInfo.ptTXTransaction)
      {
        if (NULL != ptListEntry->tListInfo.ptTXTransaction->pvComplete)
          OS_DeleteEvent(ptListEntry->tListInfo.ptTXTransaction->pvComplete);
#ifdef NXT_MULTITHREADED
        if (NULL != ptListEntry->tListInfo.ptTXTransaction->pvRefCount)
          OS_DeleteInterLockedVariable( ptListEntry->tListInfo.ptTXTransaction->pvRefCount);
#endif
        OS_Memfree( ptListEntry->tListInfo.ptTXTransaction);
      }
      if (NULL != ptListEntry->tListInfo.ptRXTransaction)
      {
        if (NULL != ptListEntry->tListInfo.ptRXTransaction->pvComplete)
          OS_DeleteEvent(ptListEntry->tListInfo.ptRXTransaction->pvComplete);
#ifdef NXT_MULTITHREADED
        if (NULL != ptListEntry->tListInfo.ptRXTransaction->pvRefCount)
          OS_DeleteInterLockedVariable( ptListEntry->tListInfo.ptRXTransaction->pvRefCount);
#endif
        OS_Memfree( ptListEntry->tListInfo.ptRXTransaction);
      }
      OS_Memfree(ptListEntry);
      ptListEntry = NULL;
    }
  }
  return ptListEntry;
}
#endif

/*****************************************************************************/
/*! Retreives a packet from a list of previous allocated buffers. If the size
does not match the pre-allocated one, a new packet will be allocated.
*   \param hTransport  Handle to transport layer instance
*   \param ulDataSize   Size of data section
*   \param usDataType   Type of packet (according to the used translation layer)
*   \return Pointer to the allocated packet                                  */
/*****************************************************************************/
PHIL_TRANSPORT_PACKET_T HilTransportAllocatePacket(NXTHANDLE hTransport, uint32_t ulDataSize, uint16_t usDataType)
{
#ifdef NXT_PACKET_PREALLOCATE
  PHIL_TRANSPORT_PACKET_T ptPacket = NULL;
  PPACKET_LISTENTRY_T     ptEntry  = NULL;

  /* pre-allocated packets are to small so allocate new packet */
  if (ulDataSize>HIL_TRANSPORT_DEFAULT_PACKET_SIZE)
  {
    if (NULL != (ptEntry = HilTransportAllocateListPacket( hTransport, ulDataSize, usDataType)))
    {
      ptEntry->tListInfo.fPartOfList = 0;
      ptPacket                       = &ptEntry->tPacket;
    }
  } else
  {
    uint32_t ulCounter = 0;

    OS_EnterLock( s_tHilTransportInst.pvPacketListLock);

    ptEntry = TAILQ_FIRST( &s_tHilTransportInst.tHilTransportPacketList);
    if ((ptEntry->tListInfo.fFree == 0) || (ptEntry->tListInfo.ptTXTransaction->fFree == 0) || (ptEntry->tListInfo.ptRXTransaction->fFree == 0))
    {
      while ((NULL != (ptEntry = TAILQ_NEXT( ptEntry, tListInfo.tList))) && (++ulCounter<HIL_TRANSPORT_PRE_ALLOC_BUFFER))
      {
        if ((ptEntry->tListInfo.fFree == 1) && (ptEntry->tListInfo.ptTXTransaction->fFree == 1) && (ptEntry->tListInfo.ptRXTransaction->fFree == 1))
        {
          ptEntry->tListInfo.fFree = 0;
          ptPacket                 = &ptEntry->tPacket;
          break;
        }
      }
    } else
    {
      ptEntry->tListInfo.fFree = 0;
      ptPacket                 = &ptEntry->tPacket;
    }
    OS_LeaveLock( s_tHilTransportInst.pvPacketListLock);
#if 0 /* enables dynamic allocation if max. pre-allocated resources are exhausted */
    if (ptEntry == NULL)
    {
      if (NULL != (ptEntry = HilTransportAllocateListPacket( hTransport, ulDataSize, usDataType)))
      {
        ptEntry->tListInfo.fPartOfList = 0;
        ptPacket                       = &ptEntry->tPacket;
      }
    }
#endif
  }
#else
  PHIL_TRANSPORT_PACKET_T ptPacket = OS_Memalloc(sizeof(*ptPacket) + sizeof(*ptPacket->ptHeader) + ulDataSize);
#endif
  /* prepare packet data */
  if (ptPacket != NULL)
  {
    OS_Memset(ptPacket, 0, sizeof(*ptPacket));
    ptPacket->hTransport = hTransport;

    ptPacket->ptHeader   = (PHIL_TRANSPORT_HEADER)(ptPacket + 1);
    OS_Memset(ptPacket->ptHeader, 0, sizeof(*ptPacket->ptHeader));
    if (ulDataSize != 0)
      ptPacket->pbData             = (uint8_t*)(ptPacket->ptHeader + 1);
    ptPacket->ulMaxSize            = ulDataSize;
    ptPacket->ptHeader->ulCookie   = HIL_TRANSPORT_COOKIE;
    ptPacket->ptHeader->usDataType = usDataType;
    ptPacket->ptHeader->usChecksum = 0; /* we don't have a checksum yet */
  }
  return ptPacket;
}

/*****************************************************************************/
/*! Frees a previous allocated HilTransportPacket (see HilTransportAllocatePacket())
*   \param ptPacket  Pointer to packet                                       */
/*****************************************************************************/
void HilTransportFreePacket( PHIL_TRANSPORT_PACKET_T ptPacket)
{
  if (ptPacket == NULL)
  {
    return;
  } else
  {
#ifdef NXT_PACKET_PREALLOCATE
    PPACKET_LISTENTRY_T ptListEntry = (PPACKET_LISTENTRY_T)LONG2PTR((PTR2LONG(ptPacket)-sizeof(struct ADMIN_Ttag)));

    if (ptListEntry)
    {
      OS_EnterLock( s_tHilTransportInst.pvPacketListLock);
      if (ptListEntry->tListInfo.fPartOfList)
      {
        /* mark packet as free */
        ptListEntry->tListInfo.fFree      = 1;
        ptPacket->ptHeader->bSequenceNr   = 0;
        ptPacket->ptHeader->usTransaction = 0;

        ptListEntry->tListInfo.ptTXTransaction->uPacket.ptSendPacket = NULL;

#if 0 /* enables dynamic allocation if max. pre-allocated resources are exhausted */
      } else
      {
        if (NULL != ptListEntry->tListInfo.ptTXTransaction)
        {
          if (NULL != ptListEntry->tListInfo.ptTXTransaction->pvComplete)
          {
            OS_DeleteEvent( ptListEntry->tListInfo.ptTXTransaction->pvComplete);
          }
          OS_Memfree(ptListEntry->tListInfo.ptTXTransaction);
        }
        OS_Memfree(ptListEntry);
#endif
      }
      OS_LeaveLock( s_tHilTransportInst.pvPacketListLock);
    }
#else
  OS_Memfree(ptPacket);
#endif
  }
}

/*****************************************************************************/
/*! Allocates and initializes receive transaction structure
*    \param  ptTransport Pointer to transport layer
*    \param  ptPacket    Pointer to send packet
*    \param  on success allocated transaction structure                     */
/****************************************************************************/
static HIL_TRANSPORT_TRANSACTION_DATA_T* HilTransportAllocateRXTransaction( PHIL_TRANSPORT_DATA_T ptTransport, PHIL_TRANSPORT_HEADER ptPacket)
{
#ifdef NXT_PACKET_PREALLOCATE
  PHIL_TRANSPORT_PACKET_T           ptTmp         = (LONG2PTR(PTR2LONG(ptPacket) - sizeof(HIL_TRANSPORT_PACKET_T)));
  PPACKET_LISTENTRY_T               ptListEntry   = (PPACKET_LISTENTRY_T)LONG2PTR((PTR2LONG(ptTmp)-sizeof(struct ADMIN_Ttag)));
  HIL_TRANSPORT_TRANSACTION_DATA_T* ptTransaction = ptListEntry->tListInfo.ptRXTransaction;

  OS_ResetEvent( ptTransaction->pvComplete);
  ptTransaction->fFree = 0;
#if 0 /* enables additional dynamic allocation if max. pre-allocated resources are exhausted */
  if (NULL == ptTmp)
  {
    if (NULL != (ptTmp = OS_Memalloc(sizeof(TRANSACTION_LISTENTRY_T)+sizeof(HIL_TRANSPORT_TRANSACTION_DATA_T))))
    {
      ptTmp->ptTransaction             = (HIL_TRANSPORT_TRANSACTION_DATA_T*)&ptTmp->ptTransaction;
      ptTmp->tListInfo.fFree           = 0;
      ptTmp->tListInfo.fPartOfList     = 0;
      ptTmp->ptTransaction->pvComplete = OS_CreateEvent();
    }
  }
#endif
#else
  HIL_TRANSPORT_TRANSACTION_DATA_T* ptTransaction = (HIL_TRANSPORT_TRANSACTION_DATA_T*)OS_Memalloc(sizeof(HIL_TRANSPORT_TRANSACTION_DATA_T));

  if (NULL != ptTransaction)
  {
    OS_Memset( ptTransaction, 0, sizeof( HIL_TRANSPORT_TRANSACTION_DATA_T));
    if (NULL == (ptTransaction->pvComplete = OS_CreateEvent()))
    {
      OS_Memfree( ptTransaction);
      return NULL;
#ifdef NXT_MULTITHREADED
    } else if (NULL == (ptTransaction->pvRefCount = OS_CreateInterLockedVariable()))
    {
      OS_DeleteEvent(ptTransaction->pvComplete);
      OS_Memfree(ptTransaction);
      return NULL;
#endif
    }
  }
#endif
  UNREFERENCED_PARAMETER( ptTransport);

  if (NULL == ptTransaction)
    return NULL;

  /* setup basic send transaction information */
  ptTransaction->uPacket.ptSendPacket = ptPacket;
  ptTransaction->fPending             = 1;
  ptTransaction->lError               = NXT_FUNCTION_FAILED;
#ifdef NXT_MULTITHREADED
  ptTransaction->fFreeData            = 0; /* @timeout handler: do not free data until we say so */
  OS_InterLockedSet( ptTransaction->pvRefCount, 0);
  ptTransaction->ulTimeOut            = 0;
#endif
  ptTransaction->fCancelled           = 0;

  return ptTransaction;
}

/*****************************************************************************/
/*! Allocates and initializes send transaction structure
*    \param  ptTransport Pointer to transport layer
*    \param  ptPacket    Pointer to send packet
*    \param  on success allocated transaction structure                     */
/****************************************************************************/
static HIL_TRANSPORT_TRANSACTION_DATA_T* HilTransportAllocateTXTransaction( PHIL_TRANSPORT_DATA_T ptTransport, PHIL_TRANSPORT_HEADER ptPacket)
{
#ifdef NXT_PACKET_PREALLOCATE
  PHIL_TRANSPORT_PACKET_T           ptTmp         = (LONG2PTR(PTR2LONG(ptPacket) - sizeof(HIL_TRANSPORT_PACKET_T)));
  PPACKET_LISTENTRY_T               ptListEntry   = (PPACKET_LISTENTRY_T)LONG2PTR((PTR2LONG(ptTmp)-sizeof(struct ADMIN_Ttag)));
  HIL_TRANSPORT_TRANSACTION_DATA_T* ptTransaction = ptListEntry->tListInfo.ptTXTransaction;

  OS_ResetEvent( ptTransaction->pvComplete);

  ptTransaction->fFree = 0;

#if 0 /* enables additional dynamic allocation if max. pre-allocated resources are exhausted */
  if (NULL == ptTmp)
  {
    if (NULL != (ptTmp = OS_Memalloc(sizeof(TRANSACTION_LISTENTRY_T)+sizeof(HIL_TRANSPORT_TRANSACTION_DATA_T))))
    {
      ptTmp->ptTransaction             = (HIL_TRANSPORT_TRANSACTION_DATA_T*)&ptTmp->ptTransaction;
      ptTmp->tListInfo.fFree           = 0;
      ptTmp->tListInfo.fPartOfList     = 0;
      ptTmp->ptTransaction->pvComplete = OS_CreateEvent();
    }
  }
#endif
#else
  HIL_TRANSPORT_TRANSACTION_DATA_T* ptTransaction = (HIL_TRANSPORT_TRANSACTION_DATA_T*)OS_Memalloc(sizeof(HIL_TRANSPORT_TRANSACTION_DATA_T));

  if (NULL != ptTransaction)
  {
    OS_Memset( ptTransaction, 0, sizeof( HIL_TRANSPORT_TRANSACTION_DATA_T));
    if (NULL == (ptTransaction->pvComplete = OS_CreateEvent()))
    {
      OS_Memfree( ptTransaction);
      return NULL;
#ifdef NXT_MULTITHREADED
    } else if (NULL == (ptTransaction->pvRefCount = OS_CreateInterLockedVariable()))
    {
      OS_DeleteEvent(ptTransaction->pvComplete);
      OS_Memfree(ptTransaction);
      return NULL;
#endif
    }
  }
#endif
  if (NULL == ptTransaction)
    return NULL;

  /* fix packet header information due to current endpoint */
  if (ptTransport->tServerInfo.fIgnoreSeqNr)
  {
    /* Old netIC Firmware might be uses a wrong checksum calculation,
       so we don't calculate the checksum */
    ptPacket->usChecksum = 0x0000;
  } else
  {
    /* Calculate packet checksum */
    ptPacket->usChecksum = CalcCrc16((uint8_t*)(ptPacket + 1), ptPacket->ulLength);
  }
  /* setup basic send transaction information */
  ptTransaction->uPacket.ptSendPacket = ptPacket;
  ptTransaction->fPending             = 1;
  ptTransaction->lError               = NXT_FUNCTION_FAILED;
#ifdef NXT_MULTITHREADED
  ptTransaction->fFreeData            = 0; /* @timeout handler: do not free data until we say so */
  ptTransaction->ulTimeOut            = 0;
  OS_InterLockedSet( ptTransaction->pvRefCount, 0);
#endif
  ptTransaction->fCancelled           = 0;

  return ptTransaction;
}

/*****************************************************************************/
/*! Frees send transaction structure, previously allocated by HilTransportAllocateTransaction()
*    \param  ptTransaction Pointer to send transaction structure            */
/****************************************************************************/
static void HilTransportFreeTransaction( HIL_TRANSPORT_TRANSACTION_DATA_T* ptTransaction)
{
#ifdef NXT_PACKET_PREALLOCATE
  ptTransaction->fFree                = 1;
  ptTransaction->uPacket.ptSendPacket = NULL;
#ifdef NXT_MULTITHREADED
  OS_InterLockedSet(ptTransaction->pvRefCount, 0);
#endif
#else
  if (NULL != ptTransaction)
  {
    if (NULL != ptTransaction->pvComplete)
      OS_DeleteEvent(ptTransaction->pvComplete);
#ifdef NXT_MULTITHREADED
    if (NULL != ptTransaction->pvRefCount)
      OS_DeleteInterLockedVariable(ptTransaction->pvRefCount);
#endif
    OS_Memfree( ptTransaction);
  }
#endif
}

#ifdef NXT_MULTITHREADED
/*****************************************************************************/
/*! Controls HilTransport Send-Thread (HilTransportSendThread())
*   \param ptPacket  Pointer to packet                                       */
/*****************************************************************************/
static void HilTransportCtlSendThread( PHIL_TRANSPORT_DATA_T ptTransport, int fRun)
{
  if (ptTransport != NULL)
  {
    if (fRun == 0)
    {
      /* wake up */
      if (ptTransport->pvSendDataSemaphore)
        OS_PutSemaphore(ptTransport->pvSendDataSemaphore, 1);
    }
    ptTransport->fRunSndThread = fRun;
  }
}

/*****************************************************************************/
/*! Controls HilTransport Send-Thread (HilTransportSendThread())
*   \param ptPacket  Pointer to packet                                       */
/*****************************************************************************/
static void HilTransportCtlTimeoutThread( PHIL_TRANSPORT_DATA_T ptTransport, int fRun)
{
  if (ptTransport != NULL)
  {
    ptTransport->fRunTimeoutThread = fRun;
  }
}

/*****************************************************************************/
/*! Frees all send packets
*   \param pvParam  Pointer to Transport Layer information                   */
/*****************************************************************************/
static void HilTransportTimeoutThread( void* pvParam)
{
  PHIL_TRANSPORT_DATA_T             ptTransport      = (PHIL_TRANSPORT_DATA_T)pvParam;
  HIL_TRANSPORT_TRANSACTION_DATA_T* ptTransaction    = NULL;

  while(ptTransport->fRunTimeoutThread)
  {
    OS_Sleep(10);
    if (ptTransport->fRunTimeoutThread == 0)
      break;

    OS_EnterLock( ptTransport->hTXTransactionsLock);

    if (NULL != (ptTransaction = TAILQ_FIRST( &ptTransport->tTXTransactions)))
    {
      do
      {
        HIL_TRANSPORT_TRANSACTION_DATA_T* ptNext = TAILQ_NEXT( ptTransaction, tList);

        /* check if packet can be freed and noone owns it */
        if (OS_InterLockedRead( ptTransaction->pvRefCount) == 0)
        {
          if ((ptTransaction->ulTimeOut == 0) || (ptTransaction->ulTimeOut <= OS_GetMilliSecCounter()))
          {
            TAILQ_REMOVE( &ptTransport->tTXTransactions, ptTransaction, tList);

            if (ptTransaction->ulTimeOut != 0)
            {
              OS_InterLockedDec( ptTransport->pvActiveSends);
            }
            if (1 == ptTransaction->fFreeData)
            {
              PHIL_TRANSPORT_PACKET_T ptTmp = (LONG2PTR(PTR2LONG(ptTransaction->uPacket.ptSendPacket) - sizeof(HIL_TRANSPORT_PACKET_T)));
              HilTransportFreePacket( ptTmp);
            }
            HilTransportFreeTransaction( ptTransaction);
          }
        }
        ptTransaction = ptNext;

      }while ((ptTransaction != NULL));
    }
    OS_LeaveLock( ptTransport->hTXTransactionsLock);
  }
}

/*****************************************************************************/
/*! HilTransport Send Thread
*   \param pvParam  Pointer to Transport Layer information                   */
/*****************************************************************************/
static void HilTransportSendThread( void* pvParam)
{
  PHIL_TRANSPORT_DATA_T             ptTransport   = (PHIL_TRANSPORT_DATA_T)pvParam;
  HIL_TRANSPORT_TRANSACTION_DATA_T* ptTransaction = NULL;
  int32_t                           lRet          = 0;
  uint32_t                          ulRet         = 0;
  NETX_CONNECTOR_FUNCTION_TABLE*    ptConnFuncs   = &ptTransport->ptConnector->tFunctions;

  if (ptTransport == NULL)
    return;

  while(ptTransport->fRunSndThread)
  {
    if (OS_SEM_SIGNALLED == (ulRet = OS_WaitSemaphore( ptTransport->pvSendDataSemaphore, OS_INFINITE_TIMEOUT)))
    {
      int fWait = 0;
      /* stop send-thread */
      if (ptTransport->fRunSndThread == 0)
        continue;

      /* get the next active transaction */
      OS_EnterLock( ptTransport->pvSendQueueLock);
      ptTransaction = TAILQ_FIRST( &ptTransport->tSendQueue);
#ifdef NXT_HILTRANSPORT_DEBUG /* update fill level of send queue */
      ptTransport->ulSendWaitCount--;
#endif
      if (NULL != ptTransaction)
      {
        TAILQ_REMOVE( &ptTransport->tSendQueue, ptTransaction, tList);
        ptTransaction->fPending = 0;
        OS_InterLockedInc( ptTransport->pvActiveSends);
        if (OS_InterLockedRead( ptTransaction->pvRefCount) < 1)
        {
          ptTransport = ptTransport;
          OS_LeaveLock( ptTransport->pvSendQueueLock);
          continue;
        }
        OS_InterLockedInc( ptTransaction->pvRefCount);
        /* update currently active send counter */
        ptTransport->ulActiveSends = OS_InterLockedRead( ptTransport->pvActiveSends);
      } else
      {
        OS_LeaveLock( ptTransport->pvSendQueueLock);
        continue;
      }
      OS_LeaveLock( ptTransport->pvSendQueueLock);

      /* initialize transaction */
      OS_EnterLock( ptTransport->hTXTransactionsLock);
      if (OS_InterLockedRead( ptTransaction->pvRefCount) == 1)/* transaction not referenced anymore, we can clean it up */
      {
        PHIL_TRANSPORT_PACKET_T ptTmp = (LONG2PTR(PTR2LONG(ptTransaction->uPacket.ptSendPacket) - sizeof(HIL_TRANSPORT_PACKET_T)));
        HilTransportFreePacket( ptTmp);
        HilTransportFreeTransaction( ptTransaction);
        OS_LeaveLock( ptTransport->hTXTransactionsLock);
        continue;
      }
      /* We need to set a unique sequence number to make sure we can assign the ACK correctly */
      if (ptTransport->bLastSequenceNr == 0)
        ptTransport->bLastSequenceNr = 1;

      OS_EnterLock(ptTransport->hRXTransactionsLock);
      ptTransaction->uPacket.ptSendPacket->bSequenceNr = ptTransport->bLastSequenceNr++;
      OS_LeaveLock(ptTransport->hRXTransactionsLock);
      if (ptTransport->usLastTransaction++ == 0x7FFF)
        ptTransport->usLastTransaction = 1;

      ptTransaction->uPacket.ptSendPacket->usTransaction = ptTransport->usLastTransaction;
      /* insert in send queue */
      TAILQ_INSERT_HEAD( &ptTransport->tTXTransactions, ptTransaction, tList);
      OS_LeaveLock( ptTransport->hTXTransactionsLock);

#ifdef NXT_HILTRANSPORT_DEBUG /* DEBUG */
      if (OS_InterLockedRead( ptTransport->pvActiveSends) > ptTransport->ulMaxParallelSends)
      {
        if(g_ulTraceLevel & TRACE_LEVEL_INFO)
        {
          USER_Trace( s_tHilTransportInst.hNetXTransportHandle, TRACE_LEVEL_INFO, "Maximum number of parallel sends since start: %d (supported %d)!", OS_InterLockedRead( ptTransport->pvActiveSends), ptTransport->tServerInfo.ulParallelServices);
        }
      }
      ptTransport->ulMaxParallelSends = NXT_MAX( ptTransport->ulMaxParallelSends, OS_InterLockedRead(ptTransport->pvActiveSends));

      /* this should never happen */
      if (OS_InterLockedRead( ptTransport->pvActiveSends)>ptTransport->tServerInfo.ulParallelServices)
      {
        if(g_ulTraceLevel & TRACE_LEVEL_ERROR)
        {
          USER_Trace( s_tHilTransportInst.hNetXTransportHandle, TRACE_LEVEL_ERROR, "Error maximum number of parallel send exceeded (supported %d) !", ptTransport->tServerInfo.ulParallelServices);
        }
      }
#endif                        /* DEBUG */
      if(NXT_NO_ERROR != (lRet = ptConnFuncs->pfnConIntfSend(ptTransport->pvConnectorInterface,
                                                              (uint8_t*)ptTransaction->uPacket.ptSendPacket,
                                                              sizeof(HIL_TRANSPORT_HEADER) + ptTransaction->uPacket.ptSendPacket->ulLength)))
      {
        /* TODO: may be we should remove it from list and put it into another, trying to resend the request */
        if(g_ulTraceLevel & TRACE_LEVEL_INFO)
        {
          USER_Trace( s_tHilTransportInst.hNetXTransportHandle, TRACE_LEVEL_INFO, "Connector-Send() failed for packet with ID: %d (Error = 0x%X)!", ptTransaction->uPacket.ptSendPacket->bSequenceNr, lRet);
        }
      }
      OS_EnterLock( ptTransport->hTXTransactionsLock);
      if (lRet != NXT_NO_ERROR)
      {
        /* send request is still active, so don't let him wait */
        if (OS_InterLockedRead( ptTransaction->pvRefCount) > 1)
        {
          ptTransaction->lError = lRet;
          /* signal waiting thread an error occured */
          if ( ptTransaction->pvComplete)
            OS_SetEvent( ptTransaction->pvComplete);
        }
      }
      /* decrement ref count */
      OS_InterLockedDec( ptTransaction->pvRefCount);
      OS_LeaveLock( ptTransport->hTXTransactionsLock);

      /* check if we have to wait, since we exceed max. number of parallel sends */
      if (OS_InterLockedRead( ptTransport->pvActiveSends)>=ptTransport->tServerInfo.ulParallelServices)
      {
#ifdef NXT_HILTRANSPORT_DEBUG /* DEBUG */
        if(g_ulTraceLevel & TRACE_LEVEL_INFO)
        {
          USER_Trace( s_tHilTransportInst.hNetXTransportHandle, TRACE_LEVEL_INFO, "Maximum number of parallel sends exceeded (supported %d). Enter wait loop!", ptTransport->tServerInfo.ulParallelServices);
        }
#endif                       /* DEBUG */
        fWait = 1;
      }
      /* enter wait loop until we deceed border */
      while(ptTransport->fRunSndThread && fWait)
      {
        OS_WaitEvent( ptTransport->pvSendEvent, 10);
        if (OS_InterLockedRead( ptTransport->pvActiveSends)<ptTransport->tServerInfo.ulParallelServices)
        {
          fWait = 0;
#ifdef NXT_HILTRANSPORT_DEBUG /* DEBUG */
          if(g_ulTraceLevel & TRACE_LEVEL_INFO)
          {
            USER_Trace( s_tHilTransportInst.hNetXTransportHandle, TRACE_LEVEL_INFO, "Deceed max. number of parallel sends (supported %d). Continue sending!", ptTransport->tServerInfo.ulParallelServices);
          }
#endif                        /* DEBUG */
        }
      }
    }
  }
}
#endif

/*****************************************************************************/
/*! HilTransport receive function. Parses incoming HilTransport data and forwards
    the data if the HilTransportPacket is complete.
*   \param pabRxBuffer  Pointer to receive buffer
*   \param ulReadLen    Size of buffer pabRxBuffer
*   \param pvUser       User data                                            */
/*****************************************************************************/
static void HilTransportRxData(uint8_t* pabRxBuffer, uint32_t ulReadLen, void* pvUser)
{
  PHIL_TRANSPORT_DATA_T ptTransport = (PHIL_TRANSPORT_DATA_T)pvUser;

  /* Parse incoming HilTransport Data and pass them to appropriate translation layer, */
  /* ACKS must be passed to HilTransportSendData to indicate send complete            */
  while(ulReadLen > 0)
  {
    switch(ptTransport->eRxState)
    {
    case eWAIT_FOR_COOKIE:
      ptTransport->tRxCurrentHeader.ulCookie <<= 8;
      ptTransport->tRxCurrentHeader.ulCookie |= *pabRxBuffer;

      ptTransport->ulRXCurrentOffset++;

      if(ptTransport->ulRXCurrentOffset >= sizeof(ptTransport->tRxCurrentHeader.ulCookie))
      {
        /* Cookie complete, so check it */
        if(HIL_TRANSPORT_COOKIE == ptTransport->tRxCurrentHeader.ulCookie)
        {
          /* Header complete */
          ptTransport->eRxState = eWAIT_FOR_HEADER;
        } else
        {
          /* Header incomplete, wait for next byte to check cookie again */
          ptTransport->ulRXCurrentOffset = 3;
        }
      }
      pabRxBuffer++;
      ulReadLen--;
      break;

    case eWAIT_FOR_HEADER:
      {
        uint32_t ulRemaining = sizeof(ptTransport->tRxCurrentHeader) - ptTransport->ulRXCurrentOffset;
        uint32_t ulCopySize  = ulRemaining;
        uint8_t* pbHeader    = (uint8_t*)&ptTransport->tRxCurrentHeader;

        if(ulCopySize > ulReadLen)
          ulCopySize = ulReadLen;

        OS_Memcpy(pbHeader + ptTransport->ulRXCurrentOffset, pabRxBuffer, ulCopySize);

        ptTransport->ulRXCurrentOffset += ulCopySize;
        pabRxBuffer                    += ulCopySize;
        ulReadLen                      -= ulCopySize;

        if(ptTransport->ulRXCurrentOffset >= sizeof(ptTransport->tRxCurrentHeader))
        {
          /* Header complete, so check it */
          if(ptTransport->tRxCurrentHeader.ulLength > 0)
          {
            ptTransport->eRxState = eWAIT_FOR_PACKET_COMPLETE;

            /* Allocate packet buffer */
            ptTransport->ptCurrentRxPacket = HilTransportAllocatePacket(ptTransport, ptTransport->tRxCurrentHeader.ulLength, 0);

            if(NULL == ptTransport->ptCurrentRxPacket)
            {
              /* Out of memory */
              if(g_ulTraceLevel & TRACE_LEVEL_ERROR)
              {
                USER_Trace( s_tHilTransportInst.hNetXTransportHandle, TRACE_LEVEL_ERROR, "Error allocating memory for Receive-Packet (Packet-ID:%d)!\n", ptTransport->tRxCurrentHeader.bSequenceNr);
              }
            } else
            {
              OS_Memcpy(ptTransport->ptCurrentRxPacket->ptHeader, &ptTransport->tRxCurrentHeader, sizeof(ptTransport->tRxCurrentHeader));
            }
          } else
          {
            switch(ptTransport->tRxCurrentHeader.usDataType)
            {
            case HIL_TRANSPORT_TYPE_ACKNOWLEDGE:
              {
                PHIL_TRANSPORT_TRANSACTION_DATA_T ptTransaction;

                /* Handle acknowledge */
                OS_EnterLock(ptTransport->hTXTransactionsLock);

                TAILQ_FOREACH(ptTransaction, &ptTransport->tTXTransactions, tList)
                {
                  if(ptTransaction->uPacket.ptSendPacket->bSequenceNr == ptTransport->tRxCurrentHeader.bSequenceNr)
                  {
#ifndef NXT_MULTITHREADED
                    TAILQ_REMOVE(&ptTransport->tTXTransactions, ptTransaction, tList);
                    ptTransaction->fPending = 0;
#endif
                    /* do not remove it from the queue this is done in a seperate thread */
                    ptTransaction->lError = cifXErrorFromTransportState(ptTransport->tRxCurrentHeader.bState);
                    if (ptTransaction->pvComplete != NULL)
                      OS_SetEvent( ptTransaction->pvComplete);

                    break;
                  }
                }

                OS_LeaveLock(ptTransport->hTXTransactionsLock);
              }
              break;

            case HIL_TRANSPORT_TYPE_KEEP_ALIVE:
              break;

            default:
              /* Invalid packet received */
              break;
            }

            /* Reset RX State machine for next packet */
            ptTransport->tRxCurrentHeader.ulCookie = 0;
            ptTransport->ulRXCurrentOffset         = 0;
            ptTransport->ulDataOffset              = 0;
            ptTransport->eRxState                  = eWAIT_FOR_COOKIE;

          }
        }
      }
      break;

    case eWAIT_FOR_PACKET_COMPLETE:
      {
        /* Store packet data */
        PHIL_TRANSPORT_PACKET_T ptRxPacket      = ptTransport->ptCurrentRxPacket;
        uint32_t                ulRemaining     = ptRxPacket->ptHeader->ulLength - ptTransport->ulDataOffset;
        uint32_t                ulCopySize      = ulRemaining;
        int                     fDispatchPacket = 1;

        if(ulCopySize > ulReadLen)
          ulCopySize = ulReadLen;

        OS_Memcpy(ptRxPacket->pbData + ptTransport->ulDataOffset, pabRxBuffer, ulCopySize);

        ulReadLen                 -= ulCopySize;
        pabRxBuffer               += ulCopySize;
        ptTransport->ulDataOffset += ulCopySize;

        if(ptTransport->ulDataOffset >= ptTransport->tRxCurrentHeader.ulLength)
        {
          /* Packet is finished, validate Checksum */
          if(ptTransport->tRxCurrentHeader.usChecksum != 0)
          {
            /* First we check against the correct CRC16 checksum. If it does not match it might
               be an old netIC Firmware which uses a wrong checksum calculation */
            if( (ptTransport->tRxCurrentHeader.usChecksum != CalcCrc16(ptRxPacket->pbData,
                                                                       ptRxPacket->ptHeader->ulLength)) &&
                (ptTransport->tRxCurrentHeader.usChecksum != CalcCrcNetIC(ptRxPacket->pbData,
                                                                         ptRxPacket->ptHeader->ulLength)) )
            {
              /* Send negative acknowledge */
              HilTransportSendAcknowledge(ptTransport, &ptTransport->tRxCurrentHeader, HIL_TSTATE_CHECKSUM_ERROR);
              fDispatchPacket = 0;
            }
          }

          if(fDispatchPacket)
          {
            PHIL_TRANSPORT_TRANSACTION_DATA_T ptTransaction;

            /* Check if this belongs to a transaction */
            OS_EnterLock(ptTransport->hRXTransactionsLock);

            TAILQ_FOREACH(ptTransaction, &ptTransport->tRXTransactions, tList)
            {
              if( (ptTransaction->uPacket.ptSendPacket->bChannel   == ptTransport->ptCurrentRxPacket->ptHeader->bChannel) &&
                  (ptTransaction->uPacket.ptSendPacket->bDevice    == ptTransport->ptCurrentRxPacket->ptHeader->bDevice) &&
                  (ptTransaction->uPacket.ptSendPacket->usDataType == ptTransport->ptCurrentRxPacket->ptHeader->usDataType) )
              {
                if (  ptTransport->tServerInfo.fIgnoreSeqNr ||
                      (ptTransaction->uPacket.ptSendPacket->bSequenceNr == ptTransport->ptCurrentRxPacket->ptHeader->bSequenceNr) )
                {
                  TAILQ_REMOVE(&ptTransport->tRXTransactions, ptTransaction, tList);

                  ptTransaction->fPending             = 0;
                  ptTransaction->uPacket.ptRecvPacket = ptTransport->ptCurrentRxPacket;

                  OS_SetEvent(ptTransaction->pvComplete);
                  fDispatchPacket = 0;
                  break;
                }
              }
            }

            OS_LeaveLock(ptTransport->hRXTransactionsLock);

            if(fDispatchPacket)
            {
              int fFreePacket = 1;

              /* Dispatch packet to default transport */
              if(ptTransport->ptDefaultTranslationLayer != NULL)
              {
                PHIL_TL_INFO_T ptDefaultTranslationLayer = ptTransport->ptDefaultTranslationLayer;

                if(ptTransport->tRxCurrentHeader.usDataType == ptDefaultTranslationLayer->ptTLInterface->usDataType)
                {
                  ptDefaultTranslationLayer->ptTLInterface->pfnReceive(ptDefaultTranslationLayer->hTLHandle,
                                                          ptTransport->ptCurrentRxPacket);
                  fFreePacket = 0;
                }
              }

              if(fFreePacket)
                HilTransportFreePacket(ptTransport->ptCurrentRxPacket);

              ptTransport->ptCurrentRxPacket = NULL;
            } else
            {
              HilTransportSendAcknowledge(ptTransport, &ptTransport->tRxCurrentHeader, 0);
            }
          } else
          {
            HilTransportFreePacket(ptTransport->ptCurrentRxPacket);
            ptTransport->ptCurrentRxPacket = NULL;
          }

          /* Reset RX State machine for next packet */
          ptTransport->tRxCurrentHeader.ulCookie = 0;
          ptTransport->ulRXCurrentOffset         = 0;
          ptTransport->ulDataOffset              = 0;
          ptTransport->eRxState                  = eWAIT_FOR_COOKIE;
        }
      }
      break;

    default:
      if(g_ulTraceLevel & TRACE_LEVEL_ERROR)
      {
        USER_Trace( s_tHilTransportInst.hNetXTransportHandle, TRACE_LEVEL_ERROR, "Error in HilTransport receive state machine! Current state (0xd) not known!", ptTransport->eRxState);
      }

      pabRxBuffer += ulReadLen;
      ulReadLen   = 0;
      break;
    }
  }

  /* store last receive time stamp */
  ptTransport->tKeepAlive.ulLastKeepAlive = OS_GetMilliSecCounter();
}

/*****************************************************************************/
/*! HILTransport Keep-Alive function. Manages Keep-Alive handling (Keep-Alive initialization,
    Keep-Alive messages, notifies interrupted connections).
*   \param hTransport            Handle to transport layer instance
*    return KEEPALIVE_STATE_E                                                 */
/******************************************************************************/
KEEPALIVE_STATE_E HilTransportHandleKeepAlive(NXTHANDLE hTransport)
{
  PHIL_TRANSPORT_DATA_T   ptTransport     = (PHIL_TRANSPORT_DATA_T)hTransport;
  PHIL_TRANSPORT_PACKET_T ptSendPacket    = NULL;
  HIL_TRANSPORT_PACKET_T* ptRecvPacket    = NULL;
  uint32_t                lRet            = 0;
  KEEPALIVE_STATE_E       eKeepAliveState = eKEEP_ALIVE_UNSUPPORTED;

  if (ptTransport->tKeepAlive.eKeepAliveState == eKEEP_ALIVE_UNSUPPORTED)
    return ptTransport->tKeepAlive.eKeepAliveState;

  switch(ptTransport->tKeepAlive.eKeepAliveState)
  {
    case eKEEP_ALIVE_UNSUPPORTED:
    {
      /* Nothing to do, as the device does not support keep alive */
    }
    break;

    case eKEEP_ALIVE_INITIALIZATION:
    {
      ptTransport->tKeepAlive.ulKeepAliveIdent = 0;

      if (NULL != (ptSendPacket = HilTransportAllocatePacket( hTransport, sizeof(uint32_t), HIL_TRANSPORT_TYPE_KEEP_ALIVE)))
      {
        ptSendPacket->ptHeader->ulLength   = sizeof(HIL_TRANSPORT_KEEPALIVE_DATA_T);
        OS_Memcpy ( ptSendPacket->pbData, &ptTransport->tKeepAlive.ulKeepAliveIdent, sizeof(HIL_TRANSPORT_KEEPALIVE_DATA_T));
        if (CIFX_NO_ERROR != (lRet = HilTransportTransferPacket( hTransport, ptSendPacket->ptHeader, &ptRecvPacket, 1000)))
        {
          if(g_ulTraceLevel & TRACE_LEVEL_INFO)
          {
            USER_Trace( s_tHilTransportInst.hNetXTransportHandle, TRACE_LEVEL_INFO, "No keep alive support for interface %s!", ptTransport->szInterfaceName);
          }
          /* We were unable to get a keep valid alive sequence number,
            so we assume the device does not support keep alive correctly */
          ptTransport->tKeepAlive.eKeepAliveState = eKEEP_ALIVE_UNSUPPORTED;

        } else
        {
          PHIL_TRANSPORT_KEEPALIVE_DATA_T ptKeepAliveData = (PHIL_TRANSPORT_KEEPALIVE_DATA_T)ptRecvPacket->pbData;

          if (ptRecvPacket->ptHeader->bState == HIL_TRANSPORT_STATE_OK)
          {
            /* Store COMId for verification */
            ptTransport->tKeepAlive.ulKeepAliveIdent   = ptKeepAliveData->ulComID;
            ptTransport->tKeepAlive.eKeepAliveState    = eKEEP_ALIVE_ACTIVE;
            ptTransport->tKeepAlive.ulKeepAliveTimeout = TRANSPORT_TO_TRANSFER;
            ptTransport->tKeepAlive.ulLastKeepAlive    = OS_GetMilliSecCounter();
          } else
          {
            if(g_ulTraceLevel & TRACE_LEVEL_INFO)
            {
              USER_Trace( s_tHilTransportInst.hNetXTransportHandle, TRACE_LEVEL_INFO, "No keep alive support for interface %s!", ptTransport->szInterfaceName);
            }
            /* error handling KeepAlive request */
            ptTransport->tKeepAlive.eKeepAliveState = eKEEP_ALIVE_UNSUPPORTED;
          }
          HilTransportFreePacket( ptSendPacket);
          if (NULL != ptRecvPacket)
            HilTransportFreePacket( ptRecvPacket);
        }
      }
    }
    break;

    case eKEEP_ALIVE_ACTIVE:
    {
      uint32_t ulTimeDiff       = OS_GetMilliSecCounter() - ptTransport->tKeepAlive.ulLastKeepAlive;
      int      fKeepAliveFailed = 0;

      if(ulTimeDiff > ptTransport->tKeepAlive.ulKeepAliveTimeout)
      {
        if (NULL != (ptSendPacket = HilTransportAllocatePacket( hTransport, sizeof(uint32_t), HIL_TRANSPORT_TYPE_KEEP_ALIVE)))
        {
          HIL_TRANSPORT_PACKET_T* ptRecvPacket = NULL;

          ptSendPacket->ptHeader->ulLength   = sizeof(HIL_TRANSPORT_KEEPALIVE_DATA_T);
          OS_Memcpy ( ptSendPacket->pbData, &ptTransport->tKeepAlive.ulKeepAliveIdent, sizeof(HIL_TRANSPORT_KEEPALIVE_DATA_T));
          if (CIFX_NO_ERROR != HilTransportTransferPacket( hTransport, ptSendPacket->ptHeader, &ptRecvPacket, 1000))
          {
            ulTimeDiff = OS_GetMilliSecCounter() - ptTransport->tKeepAlive.ulLastKeepAlive;
            /* before we notify timeout check if keep alive request failed (with timeout), because of to many pending transfers */
            if(ulTimeDiff > ptTransport->tKeepAlive.ulKeepAliveTimeout)
              fKeepAliveFailed = 1;

          } else
          {
            ptTransport->tKeepAlive.ulLastKeepAlive = OS_GetMilliSecCounter();
            HilTransportFreePacket( ptSendPacket);
          }
          if (NULL != ptRecvPacket)
          {
            HilTransportFreePacket( ptRecvPacket);
          }
        } else
        {
          fKeepAliveFailed = 1;
        }
        if (fKeepAliveFailed)
        {
          if(g_ulTraceLevel & TRACE_LEVEL_INFO)
          {
            USER_Trace( s_tHilTransportInst.hNetXTransportHandle, TRACE_LEVEL_INFO, "Stop interface %s due to failed keep alive request.", ptTransport->szInterfaceName);
          }
          /* Assume Device disconnect, as it does not answer */
          ptTransport->tKeepAlive.eKeepAliveState = eKEEP_ALIVE_TIMEOUT;
        }
      }
    }
    break;

    case eKEEP_ALIVE_TIMEOUT:
    break;

    default:
    break;
  }
  eKeepAliveState = ptTransport->tKeepAlive.eKeepAliveState;

  return eKeepAliveState;
}

/*****************************************************************************/
/*! Handles HilTransport Acknowlegde messages.
*    \param  hTransport  Handle of the transport layer instance
*    \param  ptHeader    Pointer to the HilTransport Ack-Packet header
*    \param  bState      Acknowledge state                                  */
/****************************************************************************/
void HilTransportSendAcknowledge(NXTHANDLE hTransport, PHIL_TRANSPORT_HEADER ptHeader, uint8_t bState)
{
  HIL_TRANSPORT_HEADER  tACKPacket;
  PHIL_TRANSPORT_DATA_T ptTransport = (PHIL_TRANSPORT_DATA_T)hTransport;

  OS_Memset(&tACKPacket, 0, sizeof(tACKPacket));

  tACKPacket.ulCookie      = HIL_TRANSPORT_COOKIE;
  tACKPacket.usDataType    = HIL_TRANSPORT_TYPE_ACKNOWLEDGE;
  tACKPacket.bChannel      = ptHeader->bChannel;
  tACKPacket.bDevice       = ptHeader->bDevice;
  tACKPacket.bSequenceNr   = ptHeader->bSequenceNr;
  tACKPacket.usTransaction = ptHeader->usTransaction;
  tACKPacket.bState        = bState;

  /* Send acknowledge */
  ptTransport->ptConnector->tFunctions.pfnConIntfSend( ptTransport->pvConnectorInterface,
                                                    (uint8_t*)&tACKPacket,
                                                    sizeof(tACKPacket));
}

/*****************************************************************************/
/*! Sends HilTransportPacket via the corresponding connector.
*   \param hTransport        Handle of the transport layer instance
*   \param ptPacket          Pointer to packet, to be send
*   \param ulTimeout         Timeout (to wait for acknowlegde)
*   \return NXT_NO_ERROR on success                                          */
/*****************************************************************************/
int32_t HilTransportSendPacket( NXTHANDLE hTransport, HIL_TRANSPORT_HEADER* ptPacket, uint32_t ulTimeout)
{
  PHIL_TRANSPORT_DATA_T ptTransport  = (PHIL_TRANSPORT_DATA_T)hTransport;
  uint32_t              ulConTimeout = 0;

  netXTransportGetConnectorTimeout( ptTransport->ptConnector, &ulConTimeout);

  return HilTransportSendPacketEx( hTransport, ptPacket, ulConTimeout, ulTimeout, 1);
}

#ifdef NXT_MULTITHREADED
/*****************************************************************************/
/*! Sends HilTransportPacket via the corresponding connector.
*   \param hTransport        Handle of the transport layer instance
*   \param ptPacket          Pointer to packet, to be send
*   \param ulConTimeout      Connector timeout (to wait for acknowlegde)
*   \param ulTimeout         Timeout (to wait for acknowlegde)
*   \param fCompleteTransfer == 0: does not decrement active send counter
                             since transmission is completed when answer is received
                             (see e.g. HilTransportTransferPacket())
*   \return NXT_NO_ERROR on success                                          */
/*****************************************************************************/
static int32_t HilTransportSendPacketEx( NXTHANDLE hTransport, PHIL_TRANSPORT_HEADER ptPacket, uint32_t ulConTimeout, uint32_t ulTimeout, int fCompleteTransfer)
{
  int32_t                           lRet           = NXT_OUT_OF_MEMORY;
  PHIL_TRANSPORT_DATA_T             ptTransport    = (PHIL_TRANSPORT_DATA_T)hTransport;
  HIL_TRANSPORT_TRANSACTION_DATA_T* ptTransaction  = NULL;
  int                               fDecActiveSend = 0;

  /* try to get necessary tx-transaction packet */
  if (NULL == (ptTransaction = HilTransportAllocateTXTransaction( ptTransport, ptPacket)))
  {
    return NXT_OUT_OF_MEMORY;
  }
  /************************************/
  /*** QUEUE SEND REQUEST *************/
  /************************************/
  /* queue send request in send queue */
  OS_EnterLock(ptTransport->pvSendQueueLock);
  OS_InterLockedInc( ptTransaction->pvRefCount);
  TAILQ_INSERT_TAIL(&ptTransport->tSendQueue, ptTransaction, tList);
#ifdef NXT_HILTRANSPORT_DEBUG /* monitor fill level of send queue */
  if (++ptTransport->ulSendWaitCount>ptTransport->ulSendWaitCountMax)
  {
    if(g_ulTraceLevel & TRACE_LEVEL_INFO)
    {
      USER_Trace( s_tHilTransportInst.hNetXTransportHandle, TRACE_LEVEL_INFO, "Maximum number of queued send requests %d!", ptTransport->ulSendWaitCount);
    }
  }
  ptTransport->ulSendWaitCountMax = NXT_MAX( ptTransport->ulSendWaitCountMax, ptTransport->ulSendWaitCount);
#endif
  OS_LeaveLock(ptTransport->pvSendQueueLock);

  /* notifiy send thread that a new send request is queued */
  OS_PutSemaphore( ptTransport->pvSendDataSemaphore, 1);

  if (1 == fCompleteTransfer)
    ulConTimeout += ulTimeout;
  /* wait for send becomes processed */
  if (OS_EVENT_TIMEOUT == OS_WaitEvent( ptTransaction->pvComplete, ulConTimeout))
  {
    lRet = NXT_SEND_TIMEOUT;
  } else
  {
    /* store send retval */
    lRet = ptTransaction->lError;
  }
  if (lRet != 0)
    lRet = lRet;
  /************************************/
  /*** VALIDATE TRANSACTION ***********/
  /************************************/
  /* validate how to go on (memory managment), and whether to decrement active send counter */
  OS_EnterLock(ptTransport->pvSendQueueLock);
  if (1 == ptTransaction->fPending) /* send request was never active, so remove it from the wait list and free memory */
  {
    PHIL_TRANSPORT_PACKET_T ptTmp = (LONG2PTR(PTR2LONG(ptTransaction->uPacket.ptSendPacket) - sizeof(HIL_TRANSPORT_PACKET_T)));
    TAILQ_REMOVE(&ptTransport->tSendQueue, ptTransaction, tList);
    ptTransaction->fPending = 0;
#ifdef NXT_HILTRANSPORT_DEBUG /* update fill level of send queue */
    ptTransport->ulSendWaitCount--;
#endif
    /* free data area */
    HilTransportFreePacket( ptTmp);
    OS_InterLockedDec(ptTransaction->pvRefCount);
    HilTransportFreeTransaction( ptTransaction);
    OS_LeaveLock(ptTransport->pvSendQueueLock);

  } else
  { /* transaction is currently active or already processed */
    OS_LeaveLock(ptTransport->pvSendQueueLock);

    fDecActiveSend = 1;

    /* in case of a timeout, the transaction maybe still active, so do not free the memory, instead let the timeout handler do this */
    OS_EnterLock(ptTransport->hTXTransactionsLock);
    /* somebody is still holding the transaction, so skip freeing it, and let the timeout handler do this */
    if (OS_InterLockedRead( ptTransaction->pvRefCount) > 1)
    {
      /* only free the data memory if transaction failed */
      if (lRet != 0)
        ptTransaction->fFreeData = 1; /* timeout handler will free the memory */
      /* in case of a timeout we have to add a timeout, maybe transaction started right now */
      if (lRet == NXT_SEND_TIMEOUT)
      {
        if (0 == fCompleteTransfer)
          ulConTimeout += ulTimeout;

        ptTransaction->ulTimeOut = OS_GetMilliSecCounter() + ulConTimeout;
        fDecActiveSend           = 0; /* do not decrement active send counter yet, timeout handler will do this */
      }
      OS_InterLockedDec( ptTransaction->pvRefCount);
    } else
    {
      /* remove it from the list since nobody references the transaction resources anymore */
      TAILQ_REMOVE(&ptTransport->tTXTransactions, ptTransaction, tList);
      /* in case of an error we have to cleanup data area, since nobody will access a failed request */
      if (lRet != 0)
      {
        PHIL_TRANSPORT_PACKET_T ptTmp = NULL;
        ptTmp = (LONG2PTR(PTR2LONG(ptTransaction->uPacket.ptSendPacket) - sizeof(HIL_TRANSPORT_PACKET_T)));
        HilTransportFreePacket( ptTmp);
      }
      OS_InterLockedDec( ptTransaction->pvRefCount);
      HilTransportFreeTransaction( ptTransaction);
    }
    OS_LeaveLock(ptTransport->hTXTransactionsLock);
  }
  /* decrement active send counter in case of a transfer error or if we do not estimate any response */
  if ((1 == fDecActiveSend) && ((lRet != 0) || (1 == fCompleteTransfer)))
  {
    /* trigger send thread in case we deceed max. number of parallel sends */
    if (OS_InterLockedDec( ptTransport->pvActiveSends)<ptTransport->tServerInfo.ulParallelServices)
      OS_SetEvent(ptTransport->pvSendEvent);
  }
  if (lRet != NXT_NO_ERROR)
  {
    if(g_ulTraceLevel & TRACE_LEVEL_INFO)
    {
      USER_Trace( s_tHilTransportInst.hNetXTransportHandle, TRACE_LEVEL_INFO, "HilTransportSendPacket() failed (lRet = 0x%X)!", lRet);
    }
  }
  return lRet;
}
#else
/*****************************************************************************/
/*! Sends HilTransportPacket via the corresponding connector.
*   \param hTransport  Handle of the transport layer instance
*   \param ptPacket    Pointer to packet, to be send
*   \param ulTimeout   Timeout (to wait for acknowlegde)
*   \return NXT_NO_ERROR on success                                          */
/*****************************************************************************/
static int32_t HilTransportSendPacketEx( NXTHANDLE hTransport, PHIL_TRANSPORT_HEADER ptPacket, uint32_t ulConTimeout, uint32_t ulTimeout, int fCompleteTransfer)
{
  int32_t                           lRet          = NXT_OUT_OF_MEMORY;
  PHIL_TRANSPORT_DATA_T             ptTransport   = (PHIL_TRANSPORT_DATA_T)hTransport;
  HIL_TRANSPORT_TRANSACTION_DATA_T* ptTransaction = HilTransportAllocateTXTransaction( hTransport, ptPacket);

  UNREFERENCED_PARAMETER( fCompleteTransfer);

  OS_EnterLock( ptTransport->pvSendLock);

  if (ptTransport->tServerInfo.fIgnoreSeqNr)
  {
    /* Old netIC Firmware might be uses a wrong checksum calculation,
       so we don't calculate the checksum */
    ptPacket->usChecksum = 0x0000;
  } else
  {
    /* Calculate packet checksum */
    ptPacket->usChecksum = CalcCrc16((uint8_t*)(ptPacket + 1), ptPacket->ulLength);
  }

  /* We need to set a unique sequence number to make sure we can assign the ACK correctly */
  ptPacket->bSequenceNr = ptTransport->bLastSequenceNr++;
  if (ptTransport->usLastTransaction++ == 0x7FFF)
    ptTransport->usLastTransaction = 1;

  ptPacket->usTransaction = ptTransport->usLastTransaction;

  OS_LeaveLock( ptTransport->pvSendLock);

  if(NULL != ptTransaction->pvComplete)
  {
    NETX_CONNECTOR_FUNCTION_TABLE* ptConnFuncs = &ptTransport->ptConnector->tFunctions;
    uint32_t                       ulSendSize  = sizeof(*ptPacket) + ptPacket->ulLength;

    ptTransaction->uPacket.ptSendPacket = ptPacket;
    ptTransaction->fPending             = 1;
    ptTransaction->lError               = NXT_NO_ERROR;

    OS_EnterLock(ptTransport->hTXTransactionsLock);

    TAILQ_INSERT_TAIL(&ptTransport->tTXTransactions, ptTransaction, tList);

    if(NXT_NO_ERROR != (lRet = ptConnFuncs->pfnConIntfSend(ptTransport->pvConnectorInterface,
                                                        (uint8_t*)ptPacket,
                                                        ulSendSize)))
    {
      if(g_ulTraceLevel & TRACE_LEVEL_INFO)
      {
        USER_Trace( s_tHilTransportInst.hNetXTransportHandle, TRACE_LEVEL_INFO, "Connector-Send() failed for packet with ID: %d (Error = 0x%X)!", ptPacket->bSequenceNr, lRet);
      }

      TAILQ_REMOVE(&ptTransport->tTXTransactions, ptTransaction, tList);
    }

    OS_LeaveLock(ptTransport->hTXTransactionsLock);

    if(NXT_NO_ERROR == lRet)
    {
      /* Wait for ACK */
      if(OS_EVENT_SIGNALLED == OS_WaitEvent( ptTransaction->pvComplete, ulTimeout+ulConTimeout))
      {
        /* We got the event, so this transaction is queued out and the error code has been set */
        lRet = ptTransaction->lError;

      } else
      {
        /* Timeout, so return error and dequeue transaction */
        OS_EnterLock(ptTransport->hTXTransactionsLock);

        /* Only remove it from list, if it's still pending */
        if(ptTransaction->fPending)
          TAILQ_REMOVE(&ptTransport->tTXTransactions, ptTransaction, tList);

        OS_LeaveLock(ptTransport->hTXTransactionsLock);

        lRet = NXT_SEND_TIMEOUT;
      }
    }
    /* free send packet */
    if (lRet != NXT_NO_ERROR)
    {
      PHIL_TRANSPORT_PACKET_T ptTmp = (LONG2PTR(PTR2LONG(ptTransaction->uPacket.ptSendPacket) - sizeof(HIL_TRANSPORT_PACKET_T)));

      HilTransportFreePacket( ptTmp);
    }
  }
  HilTransportFreeTransaction( ptTransaction);

  return lRet;
}
#endif

/*****************************************************************************/
/*! Transfers HilTransportPacket (waits for response).
*   \param hTransport     Handle of the transport layer instance
*   \param ptSendPacket   Pointer to packet, to be send
*   \param pptRecvPacket  Reference to pointer for receive packet
*   \param ulTimeout      Timeout (to wait for response)
*   \return NXT_NO_ERROR on success                                          */
/*****************************************************************************/
int32_t HilTransportTransferPacket(NXTHANDLE hTransport, HIL_TRANSPORT_HEADER* ptSendPacket, PHIL_TRANSPORT_PACKET_T* pptRecvPacket, uint32_t ulTimeout)
{
  int32_t                           lRet            = NXT_OUT_OF_MEMORY;
  PHIL_TRANSPORT_DATA_T             ptTransport     = (PHIL_TRANSPORT_DATA_T)hTransport;
  HIL_TRANSPORT_TRANSACTION_DATA_T* ptRXTransaction = NULL;

  if (NULL != (ptRXTransaction = HilTransportAllocateRXTransaction( hTransport, ptSendPacket)))
  {
    uint32_t ulConTimeout = 0;

    OS_EnterLock(ptTransport->hRXTransactionsLock);
    TAILQ_INSERT_TAIL(&ptTransport->tRXTransactions, ptRXTransaction, tList);
    OS_LeaveLock(ptTransport->hRXTransactionsLock);

    netXTransportGetConnectorTimeout( ptTransport->ptConnector, &ulConTimeout);

    if(NXT_NO_ERROR == (lRet = HilTransportSendPacketEx(hTransport, ptSendPacket, ulConTimeout, ulTimeout, 0)))
    {
      if(OS_EVENT_TIMEOUT == OS_WaitEvent(ptRXTransaction->pvComplete, ulTimeout + ulConTimeout))
      {
        lRet = NXT_RECV_TIMEOUT;
      } else
      {
        if (ptRXTransaction->fCancelled == 0)
        {
          *pptRecvPacket = ptRXTransaction->uPacket.ptRecvPacket;
        } else
        {
          lRet = NXT_TRANSACTION_CANCELLED;
        }
      }
#ifdef NXT_MULTITHREADED
      /* request was sent and transmission is processed (independent of the ret val), so decrement active send counter */
      if (OS_InterLockedDec(ptTransport->pvActiveSends)<ptTransport->tServerInfo.ulParallelServices)
      {
        OS_SetEvent(ptTransport->pvSendEvent);
      }
#endif
    }
    /* if transaction is still pending remove it from the list */
    OS_EnterLock(ptTransport->hRXTransactionsLock);
    if(ptRXTransaction->fPending)
    {
      /* remove from queue transaction is was not answered */
      TAILQ_REMOVE(&ptTransport->tRXTransactions, ptRXTransaction, tList);
    } else
    {
      /* in case of an error and if we got an answer we have to free the data right now */
      if ((lRet != 0) && (NULL != ptRXTransaction->uPacket.ptRecvPacket) && (ptRXTransaction->uPacket.ptRecvPacket != (PHIL_TRANSPORT_PACKET_T)ptSendPacket))
        HilTransportFreePacket( ptRXTransaction->uPacket.ptRecvPacket);
    }
    OS_LeaveLock(ptTransport->hRXTransactionsLock);

    HilTransportFreeTransaction( ptRXTransaction);
  }
  return lRet;
}

/*****************************************************************************/
/*! Queries the server information.
*   \param ptTransport   Pointer to transport layer information structure
*   \param ptServerInfo  Pointer to server information structure
*   \return NXT_NO_ERROR on success                                          */
/*****************************************************************************/
static int32_t HilTransportQueryServerData( PHIL_TRANSPORT_DATA_T ptTransport, PHIL_TRANSPORT_ADMIN_QUERYSERVER_DATA_T* pptServerInfo)
{
  int32_t                 lRet         = -1;
  PHIL_TRANSPORT_PACKET_T ptRecvPacket = NULL;
  HIL_TRANSPORT_HEADER*   ptQuery      = NULL;
  PHIL_TRANSPORT_PACKET_T ptSendPacket = NULL;

  *pptServerInfo = NULL;
  if (NULL == (ptSendPacket = HilTransportAllocatePacket( ptTransport, 0, HIL_TRANSPORT_TYPE_QUERYSERVER)))
    return NXT_NO_MEMORY;

  ptQuery = ptSendPacket->ptHeader;

  ptQuery->ulCookie      = HIL_TRANSPORT_COOKIE;
  ptQuery->usDataType    = HIL_TRANSPORT_TYPE_QUERYSERVER;
  ptQuery->usTransaction = 1;

  if(NXT_NO_ERROR == (lRet = HilTransportTransferPacket( ptTransport, ptQuery, &ptRecvPacket, TRANSPORT_TO_QUERY_SERVER)))
  {
    lRet = -1;
    if (ptRecvPacket->ptHeader->ulLength >= sizeof(HIL_TRANSPORT_ADMIN_QUERYSERVER_DATA_T))
    {
      if(NULL != (*pptServerInfo = OS_Memalloc(ptRecvPacket->ptHeader->ulLength)))
      {
        OS_Memcpy( *pptServerInfo, ptRecvPacket->pbData, ptRecvPacket->ptHeader->ulLength);
        lRet = NXT_NO_ERROR;
      }
    }
    HilTransportFreePacket( ptSendPacket);
  }

  if(NULL != ptRecvPacket)
    HilTransportFreePacket(ptRecvPacket);

  return lRet;
}

/*****************************************************************************/
/*! Calculate CRC16 checksum for HIL_TRANSPORT_HEADER
 *   \param  pbData     Data pointer
 *   \param  ulDataLen  Length of data
 *   \return CRC16 Checksum                                                 */
/****************************************************************************/
static uint16_t CalcCrc16(uint8_t* pbData, uint32_t ulDataLen)
{
  /* CRC16-CCIT Polynom */
  uint16_t usCrcValue = 0xFFFF;
  uint32_t ulIdx;

  for(ulIdx = 0; ulIdx < ulDataLen; ulIdx++)
  {
    usCrcValue  = (usCrcValue >> 8) | (usCrcValue << 8);
    usCrcValue ^= pbData[ulIdx];
    usCrcValue ^= (usCrcValue & 0xFF) >> 4;
    usCrcValue ^= (usCrcValue << 8) << 4;
    usCrcValue ^= ((usCrcValue & 0xFF) << 4) << 1;
  }
  return ~usCrcValue;
}

/****************************************************************************/
/*! This is a checksum used on first release netIC Firmware version. It is NO Crc checksum,
 *  but we need it to be "old" compatible.
 *   \param  pbData     Data pointer
 *   \param  ulDataLen  Length of data
 *   \return netIC Checksum                                                 */
/****************************************************************************/
static uint16_t CalcCrcNetIC(uint8_t* pbData, uint32_t ulDataLen)
{
  uint16_t usCrcValue = 0xFFFF;
  uint32_t ulIdx;

  #define lo8(_x_) (uint8_t)(_x_ & 0xFF)
  #define hi8(_x_) (uint8_t)((_x_ >> 8) & 0xFF)

  for(ulIdx = 0; ulIdx < ulDataLen; ulIdx++)
  {
    uint8_t data = pbData[ulIdx];

    data ^= lo8(usCrcValue);
    data ^= data << 4;

    usCrcValue = ((((uint16_t)data << 8) | hi8(usCrcValue)) ^ (uint8_t)(data >> 4) ^ ((uint16_t)data << 3));
  }

  return usCrcValue;
}

/****************************************************************************/
/*! Create a CIFX error from a HIL transport state
 *   \param  bState  State from the HIL transport header
 *   \return corresponding CIFX error code for bState                       */
/****************************************************************************/
static int32_t cifXErrorFromTransportState(uint8_t bState)
{
  int32_t lRet;

  switch(bState)
  {
  case HIL_TRANSPORT_STATE_OK:
    lRet = CIFX_NO_ERROR;
    break;

  case HIL_TSTATE_CHECKSUM_ERROR:
    lRet = CIFX_TRANSPORT_CHECKSUM_ERROR;
    break;

  case HIL_TSTATE_LENGTH_INCOMPLETE:
    lRet = CIFX_TRANSPORT_LENGTH_INCOMPLETE;
    break;

  case HIL_TSTATE_DATA_TYPE_UNKNOWN:
    lRet = CIFX_TRANSPORT_DATA_TYPE_UNKOWN;
    break;

  case HIL_TSTATE_DEVICE_UNKNOWN:
    lRet = CIFX_TRANSPORT_DEVICE_UNKNOWN;
    break;

  case HIL_TSTATE_CHANNEL_UNKNOWN:
    lRet = CIFX_TRANSPORT_CHANNEL_UNKNOWN;
    break;

  case HIL_TSTATE_SEQUENCE_ERROR:
    lRet = CIFX_TRANSPORT_SEQUENCE;
    break;

  case HIL_TSTATE_BUFFEROVERFLOW_ERROR:
    lRet = CIFX_TRANSPORT_BUFFEROVERFLOW;
    break;

  case HIL_TSTATE_KEEP_ALIVE_ERROR:
    lRet = CIFX_TRANSPORT_KEEPALIVE;
    break;

  case HIL_TSTATE_RESOURCE_ERROR:
    lRet = CIFX_TRANSPORT_RESOURCE;
    break;

  default:
    lRet = CIFX_TRANSPORT_ERROR_UNKNOWN;
    break;
  }

  return lRet;
}

/****************************************************************************/
/*! Returns information of current state of a transport layer
 *   \param  NXTHANDLE       Handle to transport layer
 *   \return ptTransportInfo Pointer to transport information structure     */
/****************************************************************************/
int32_t HilTransportGetTransportInfo( NXTHANDLE hTransport, HIL_TRANSPORT_STATUS_INFO_T* ptTransportInfo)
{
  PHIL_TRANSPORT_DATA_T ptTransport = (PHIL_TRANSPORT_DATA_T)hTransport;

  if ((ptTransportInfo == NULL) || (ptTransport == NULL))
    return NXT_INVALID_PARAMETER;

  if (ptTransport->tServerInfo.ptQueryServerData != NULL)
  {
    OS_Memcpy(&ptTransportInfo->tTransportServerInfo, ptTransport->tServerInfo.ptQueryServerData,sizeof(HIL_TRANSPORT_STATUS_INFO_T));
  } else
  {
    ptTransportInfo->tTransportServerInfo.ulBufferSize       = ptTransport->tServerInfo.ulBufferSize;
    ptTransportInfo->tTransportServerInfo.ulParallelServices = ptTransport->tServerInfo.ulParallelServices;
    ptTransportInfo->tTransportServerInfo.ulFeatures         = ptTransport->tServerInfo.ulFeatures;
  }
  ptTransportInfo->ulActiveSends = ptTransport->ulActiveSends;

  return NXT_NO_ERROR;
}
