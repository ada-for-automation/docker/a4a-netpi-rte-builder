/**************************************************************************************
 
   Copyright (c) Hilscher GmbH. All Rights Reserved.
 
 **************************************************************************************
 
   Filename:
    $Workfile: $
   Last Modification:
    $Author: MichaelT $
    $Modtime: $
    $Revision: 1034 $
   
   Targets:
     Win32/ANSI   : yes
     Win32/Unicode: yes (define _UNICODE)
     WinCE        : yes
 
   Description:
   Win32 OS Abstraction Layer implementation.
       
   Changes:
 
     Version   Date        Author   Description
     ----------------------------------------------------------------------------------
     1         ?           ?        ?
 
**************************************************************************************/

#include "OS_includes.h"

CRITICAL_SECTION g_tcsQueueLock;

int OS_LOCK(void)
{
  EnterCriticalSection(&g_tcsQueueLock);
  return 1;
}

void OS_UNLOCK(int iLock)
{
  UNREFERENCED_PARAMETER(iLock);
  LeaveCriticalSection(&g_tcsQueueLock);
}
