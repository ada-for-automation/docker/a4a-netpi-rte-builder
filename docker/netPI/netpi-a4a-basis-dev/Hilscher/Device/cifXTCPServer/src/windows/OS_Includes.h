#ifndef __OS_INCLUDES__H
#define __OS_INCLUDES__H

#include "Windows.h"

#include <string.h>
#include <stdlib.h>
#include <malloc.h>

#ifndef NULL
  #define NULL  ((void*)0)
#endif

#define OS_MALLOC     malloc
#define OS_FREE       free
#define OS_MEMCPY     memcpy
#define OS_STRNICMP   strnicmp
#define OS_STRNCPY    strncpy

int  OS_LOCK(void);
void OS_UNLOCK(int iLock);

#define OS_GETTICKCOUNT GetTickCount

/* SLIST_ENTRY is also defined in Windows.h, so we need to undefine it here */
#undef SLIST_ENTRY

#endif /* __OS_INCLUDES__H */
