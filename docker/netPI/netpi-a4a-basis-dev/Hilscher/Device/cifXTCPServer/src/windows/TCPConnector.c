/**************************************************************************************

   Copyright (c) Hilscher GmbH. All Rights Reserved.

 **************************************************************************************

   Filename:
    $Id: TCPConnector.cpp 1917 2010-07-16 15:51:16Z stephans $
   Last Modification:
    $Author: stephans $
    $Date: 2010-07-16 17:51:16 +0200 (Fr, 16 Jul 2010) $
    $Revision: 1917 $

   Targets:
     Win32        : yes

   Description:
    TCP/IP connector for Hilscher marshaller package

   Changes:

     Version   Date        Author   Description
     ----------------------------------------------------------------------------------
     3        15.04.2010   SS       - Timeout handling added to close client after 5 
                                      seconds of inactivity
                                    - Bugfix: Application crashes if loading of cifX 
                                      device driver failed
     2        04.12.2009   SS       - Query client name
                                    - Show TCP traffic via timer event
     1        25.05.2009   MT       initial version

**************************************************************************************/

//#include "stdafx.h"
//#include "OS_Includes.h"
#include "Winsock2.h"
#include "Ws2tcpip.h"

#undef SLIST_ENTRY
#include "MarshallerInternal.h"
#include "TCPConnector.h"

/*****************************************************************************/
/*! Internal UART connector data                                             */
/*****************************************************************************/
typedef struct TCP_CONN_INTERNAL_Ttag
{
  TLR_UINT32 ulConnectorIdx;
  void*      pvMarshaller;

  int        fRunning;

  SOCKET     hListen;
  HANDLE     hServerThread;
  SOCKET     hClient;
  HANDLE     hClientThread;
} TCP_CONN_INTERNAL_T;

static WSADATA              s_tWsaData;
static int                  s_iWinsockInitialized = 0;
static TCP_CONN_INFO_T      s_tTcpInfo            = {0};

/*****************************************************************************/
/*! Function called from marshaller when data is to be sent to interface
*   \param ptBuffer   Buffer to send
*   \param pvUser     TCP connector internal data                            */
/*****************************************************************************/
static TLR_RESULT TCPConnectorSend(HIL_MARSHALLER_BUFFER_T* ptBuffer, void* pvUser)
{
  TCP_CONN_INTERNAL_T* ptTcpData = (TCP_CONN_INTERNAL_T*)pvUser;
  unsigned long        ulDataLen = sizeof(ptBuffer->tTransport) + 
                                   ptBuffer->tMgmt.ulUsedDataBufferLen;

  s_tTcpInfo.ulTxCount += ulDataLen;

  send(ptTcpData->hClient,
       (char*)&ptBuffer->tTransport,
       ulDataLen,
       0);

  HilMarshallerConnTxComplete(ptBuffer->tMgmt.pvMarshaller, 
                              ptTcpData->ulConnectorIdx, 
                              ptBuffer);

  return TLR_S_OK;
}

/*****************************************************************************/
/*! TCP connector uninitialization
*   \param pvUser Pointer to internal connector data                         */
/*****************************************************************************/
static void TCPConnectorDeinit(void* pvUser)
{
  TCP_CONN_INTERNAL_T* ptTcpData = (TCP_CONN_INTERNAL_T*)pvUser;

  /* Check if data is valid */
  if(NULL != ptTcpData)
  {
    HANDLE hClientThread = ptTcpData->hClientThread;
    ptTcpData->fRunning  = 0;
    
    if(NULL != hClientThread)
    {
      if(WAIT_OBJECT_0 != WaitForSingleObject(hClientThread, 2000))
        TerminateThread(hClientThread, MAXDWORD);

      CloseHandle(hClientThread);     
    }

    if(INVALID_SOCKET != ptTcpData->hClient)
      closesocket(ptTcpData->hClient);

    if(NULL != ptTcpData->hServerThread)
    {
      if(WAIT_OBJECT_0 != WaitForSingleObject(ptTcpData->hServerThread, 2000))
        TerminateThread(ptTcpData->hServerThread, MAXDWORD);

      CloseHandle(ptTcpData->hServerThread);     
    }

    if(INVALID_SOCKET != ptTcpData->hListen)
      closesocket(ptTcpData->hListen);

    /* Unregister from Marshaller */
    if(ptTcpData->ulConnectorIdx != ~0)
    {
      HilMarshallerUnregisterConnector(ptTcpData->pvMarshaller, ptTcpData->ulConnectorIdx);
    }

    free(ptTcpData);
  }

  if(--s_iWinsockInitialized == 0)
  {
    WSACleanup();
  }
}

/*****************************************************************************/
/*! Thread handling connection with client
*   \param pvParam Pointer reference to TCP connection structure
*   \return        Always 0                                                  */
/*****************************************************************************/
static DWORD WINAPI ClientThread(void* pvParam)
{
  TCP_CONN_INTERNAL_T* ptTcpData = (TCP_CONN_INTERNAL_T*)pvParam;
  
  while(ptTcpData->fRunning)
  {
    FD_SET  tRead, tExcept;
    TIMEVAL tTimeout = {0};

    tTimeout.tv_sec = 5;

    FD_ZERO(&tRead);
    FD_ZERO(&tExcept);

    FD_SET(ptTcpData->hClient, &tRead);
    FD_SET(ptTcpData->hClient, &tExcept);

    
    int iErr = select(0, &tRead, NULL, &tExcept, &tTimeout);
    if(0 < iErr)
    {
      if(FD_ISSET(ptTcpData->hClient, &tRead))
      {
        /* We have data to read */
        unsigned long  ulDataLen;
        unsigned char* pbData;
        int            iRecv;

        ioctlsocket(ptTcpData->hClient, FIONREAD, &ulDataLen);
        pbData    = (unsigned char*)malloc(ulDataLen);

        iRecv = recv(ptTcpData->hClient, (char*)pbData, ulDataLen, 0);

        if( (SOCKET_ERROR == iRecv) ||
            (0            == iRecv) )
        {
          /* Gracefully closed socket */
          ptTcpData->hClient = INVALID_SOCKET;
          free(pbData);
          break;

        } else
        {
          unsigned long ulDataLen = (unsigned long)iRecv;

          s_tTcpInfo.ulRxCount += ulDataLen;
          
          HilMarshallerConnRxData(ptTcpData->pvMarshaller,
                                  ptTcpData->ulConnectorIdx,
                                  pbData,
                                  ulDataLen);
        }

        free(pbData);
      }

      if(FD_ISSET(ptTcpData->hClient, &tExcept))
      {
        /* Socket has been closed */
        ptTcpData->hClient = INVALID_SOCKET;
        break;
      }
    } else if (iErr == 0)
    {
      /* Timeout -> close socket */
      closesocket(ptTcpData->hClient);
      ptTcpData->hClient = INVALID_SOCKET;
      break;
    }
  }
  
  s_tTcpInfo.fConnected = 0;
  ptTcpData->hClientThread = NULL;

  return 0;
}

/*****************************************************************************/
/*! Thread serving incoming TCP connections
*   \param pvParam Pointer reference to TCP connection structure
*   \return        Always 0                                                  */
/*****************************************************************************/
static DWORD WINAPI ServerThread(void* pvParam)
{
  TCP_CONN_INTERNAL_T* ptTcpData = (TCP_CONN_INTERNAL_T*)pvParam;

  while(ptTcpData->fRunning)
  {
    FD_SET  tRead;
    TIMEVAL tTimeout = {0};

    tTimeout.tv_sec = 1;

    FD_ZERO(&tRead);
    FD_SET(ptTcpData->hListen, &tRead);

    if(0 < select(0, &tRead, NULL, NULL, &tTimeout))
    {
      if(FD_ISSET(ptTcpData->hListen, &tRead))
      {
        /* We have a client to accept */
        SOCKADDR_IN tSockAddr    = {0};
        int         iSockAddrLen = sizeof(tSockAddr);
        SOCKET      hClient      = accept(ptTcpData->hListen, (SOCKADDR*)&tSockAddr, &iSockAddrLen);

        if(ptTcpData->hClient != INVALID_SOCKET)
        {
          /* We already have a client, so reject this one */
          closesocket(hClient);
        } else
        {
          char  szHostname[NI_MAXHOST];
          BOOL  fNoDelay = TRUE;

          /* Populate TCP info structure */
          strncpy(s_tTcpInfo.szIPAddress, inet_ntoa(tSockAddr.sin_addr), sizeof(s_tTcpInfo.szIPAddress));
          s_tTcpInfo.fConnected = 1;
          s_tTcpInfo.ulRxCount  = 0;
          s_tTcpInfo.ulTxCount  = 0;
          
          if (0 == getnameinfo( (struct sockaddr *) &tSockAddr, sizeof (struct sockaddr), 
                                szHostname,NI_MAXHOST, NULL, 0, 0))
          {
            strncpy(s_tTcpInfo.szHostname, szHostname, sizeof(s_tTcpInfo.szHostname));
          } else
          {
            strncpy(s_tTcpInfo.szHostname, "unknown", sizeof(s_tTcpInfo.szHostname));
          }

          setsockopt(hClient, IPPROTO_TCP, TCP_NODELAY, 
                     (const char*)&fNoDelay, sizeof(fNoDelay));

          ptTcpData->hClient       = hClient;
          ptTcpData->hClientThread = CreateThread(NULL, 0, ClientThread, ptTcpData, 0, NULL);
        }
      }
    }
  }

  return 0;
}

/*****************************************************************************/
/*! TCP connector initialization
*   \param ptParams     Marshaller specific parameters (e.g. timeout)
*   \param ptConfigData UART configuration data
*   \param pvMarshaller Handle to the marshaller, this connector should be added
*   \return TLR_S_OK on success                                              */
/*****************************************************************************/
TLR_RESULT TCPConnectorInit(const HIL_MARSHALLER_CONNECTOR_PARAMS_T* ptParams, void* pvMarshaller)
{
  HIL_MARSHALLER_CONNECTOR_T tMarshConn;
  TLR_RESULT                 eRet;
  TCP_CONN_INTERNAL_T*       ptTcpData = NULL;

  if(++s_iWinsockInitialized == 1)
  {
    /* We are the first to initialize Winsock */
    if(0 != WSAStartup(MAKEWORD(1,0), &s_tWsaData))
    {
      s_iWinsockInitialized--;
      return HIL_MARSHALLER_E_OUTOFRESOURCES;
    }
  }

  if(NULL == (ptTcpData = (TCP_CONN_INTERNAL_T*)malloc(sizeof(*ptTcpData))))
  {
    eRet = HIL_MARSHALLER_E_OUTOFMEMORY;
  } else
  {
    SOCKADDR_IN tSockAddr = {0};

    memset(&tMarshConn, 0, sizeof(tMarshConn));
    memset(ptTcpData, 0, sizeof(*ptTcpData));

    ptTcpData->ulConnectorIdx = (TLR_UINT32)~0;
    ptTcpData->pvMarshaller   = pvMarshaller;
    ptTcpData->hClient        = INVALID_SOCKET;
    ptTcpData->fRunning       = 1;

    tSockAddr.sin_addr.s_addr = INADDR_ANY;

    unsigned short usPortAddr = HIL_TRANSPORT_IP_PORT;
    _swab( (char*)&usPortAddr, (char*)&tSockAddr.sin_port, 2);// htons() does not work under CE

 
    tSockAddr.sin_family      = AF_INET;

    if(INVALID_SOCKET == (ptTcpData->hListen = socket(AF_INET, SOCK_STREAM, 0)))
    {
      eRet = HIL_MARSHALLER_E_OUTOFRESOURCES;

    } else if(SOCKET_ERROR == bind(ptTcpData->hListen, (SOCKADDR*)&tSockAddr, sizeof(tSockAddr)))
    {
      eRet = HIL_MARSHALLER_E_OUTOFRESOURCES;

    } else if(SOCKET_ERROR == listen(ptTcpData->hListen, 0))
    {
      eRet = HIL_MARSHALLER_E_OUTOFRESOURCES;

    } else if(NULL == (ptTcpData->hServerThread = CreateThread(NULL,
                                                               0,
                                                               ServerThread,
                                                               ptTcpData,
                                                               0,
                                                               NULL)))
    {
      eRet = HIL_MARSHALLER_E_OUTOFRESOURCES;

    } else
    {
      tMarshConn.pfnTransmit      = TCPConnectorSend;
      tMarshConn.pfnDeinit        = TCPConnectorDeinit;
      tMarshConn.pvUser           = ptTcpData;
      tMarshConn.ulDataBufferSize = ptParams->ulDataBufferSize;
      tMarshConn.ulDataBufferCnt  = ptParams->ulDataBufferCnt;
      tMarshConn.ulTimeout        = ptParams->ulTimeout;

      eRet = HilMarshallerRegisterConnector(pvMarshaller, 
                                            &ptTcpData->ulConnectorIdx, 
                                            &tMarshConn);

    }

    /* Something has failed, so uninitialize this connector instance */
    if(eRet != TLR_S_OK)
    {
      TCPConnectorDeinit(ptTcpData);
    }
  }
  
  return eRet;
}

/*****************************************************************************/
/*! Returns TCP connector information
*   \param  ptTcpInfo Buffer for TCP connector information
*   \return TLR_S_OK on success                                              */
/*****************************************************************************/
TLR_RESULT TCPConnectorInfo(TCP_CONN_INFO_T* ptTcpInfo)
{
  TLR_RESULT eRet = TLR_S_OK;  
  *ptTcpInfo = s_tTcpInfo;
  return eRet;
}

/*****************************************************************************/
/*! Returns Local IP address
*   \param  pszIPAddress Buffer for local ip address
*   \param  pszIPAddress Len of buffer
*   \return TLR_S_OK on success                                              */
/*****************************************************************************/
TLR_RESULT TCPConnectorGetLocalIP(char* pszIPAddress, int iLen)
{
  TLR_RESULT eRet = TLR_S_OK;  
  char       szHostName[256];
  
  memset(szHostName, 0x00, sizeof(szHostName));

  if (gethostname(szHostName, sizeof(szHostName)) == SOCKET_ERROR)
  {
    eRet = HIL_MARSHALLER_E_OUTOFRESOURCES;
  } else
  {
    struct hostent *he = gethostbyname(szHostName);
    if (he == NULL)
    {
      eRet = HIL_MARSHALLER_E_OUTOFRESOURCES;
    }
    else
    {
      struct in_addr addr;
      char*  pszIP = NULL;
      memcpy(&addr, he->h_addr_list[0], sizeof(struct in_addr));
      pszIP = inet_ntoa(addr);
      strncpy(pszIPAddress, pszIP, iLen); 
    }
  }
  
  return eRet;
}
