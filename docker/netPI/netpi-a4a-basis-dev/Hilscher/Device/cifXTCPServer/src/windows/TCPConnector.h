/**************************************************************************************

   Copyright (c) Hilscher GmbH. All Rights Reserved.

 **************************************************************************************

   Filename:
    $Id: TCPConnector.h 1917 2010-07-16 15:51:16Z stephans $
   Last Modification:
    $Author: stephans $
    $Date: 2010-07-16 17:51:16 +0200 (Fr, 16 Jul 2010) $
    $Revision: 1917 $

   Targets:
     Win32        : yes

   Description:
    TCP/IP connector for Hilscher marshaller package

   Changes:

     Version   Date        Author   Description
     ----------------------------------------------------------------------------------
     1        25.05.2009   MT       initial version

**************************************************************************************/


#ifndef __TCPCONNECTOR__H
#define __TCPCONNECTOR__H

#include "MarshallerConfig.h"

/*****************************************************************************/
/*! TCP connector information data                                           */
/*****************************************************************************/
typedef struct TCP_CONN_INFOtag
{
  int           fConnected;
  char          szHostname[64];
  char          szIPAddress[15];
  unsigned long ulRxCount;
  unsigned long ulTxCount;

} TCP_CONN_INFO_T;

TLR_RESULT TCPConnectorInit       (const HIL_MARSHALLER_CONNECTOR_PARAMS_T* ptParams, void* pvMarshaller);
TLR_RESULT TCPConnectorInfo       (TCP_CONN_INFO_T* ptTcpInfo);
TLR_RESULT TCPConnectorGetLocalIP (char* pszIPAddress, int iLen);

#endif /* __TCPCONNECTOR__H */