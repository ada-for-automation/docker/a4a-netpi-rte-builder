/**************************************************************************************
 
   Copyright (c) Hilscher GmbH. All Rights Reserved.
 
 **************************************************************************************
 
   Filename:
    $Workfile: $
   Last Modification:
    $Author: stephans $
    $Modtime: $
    $Revision: 2433 $
   
   Targets:
     Win32/ANSI   : yes
     Win32/Unicode: yes (define _UNICODE)
 
   Description:
   cifX API function implementation
       
   Changes:
 
     Version   Date        Author   Description
     ----------------------------------------------------------------------------------
     4         13.12.2010  SS       - Download hook added
     3         29.01.2010  SS       - Add FindFirstFile/FindNextFile driver functions 
                                      to marshaller configuration 
                                    - Add doxygen comments
     2         04.12.2009  SS       - Wrapper for xSysdeviceOpen(),xSysdeviceClose()
                                      xChannelOpen(),xChannelClose() added to track
                                      channel access
                                    - Name of connected client added to list
                                    - GUI design changed
                                    - Use CTreeListCtrl to display clients
     1         ?           ?        ?
 
**************************************************************************************/

#include <windows.h>

#include "TCPConnector.h"
#include "TCPServer.h"
#include "CifXTransport.h"
#include "cifXErrors.h"
#include "cifXUser.h"
#include "cifXDownloadHook.h"
#include "netXAPI.h"

#include "a4a_cifXAPIWrap.h"
#include "a4a_cifXAPIWrapAda.h"


extern CRITICAL_SECTION g_tcsQueueLock;

/* marshaller handle */
void*            g_pvMarshaller = NULL;

/*****************************************************************************/
/*! This function is called after each interval specified in the setitimer
 *  function (see InitMarshaller) used to install a timer.
 *    \param iSignal   Signal which caused the call                          */
/*****************************************************************************/
void MarshallerTimer(void)
{
  HilMarshallerTimer(g_pvMarshaller);
}

/*****************************************************************************/
/*! Function for Marshaller Request                                          */
/*****************************************************************************/
void MarshallerRequest(void* pvMarshaller, void* pvUser)
{
  UNREFERENCED_PARAMETER(pvUser);
  HilMarshallerMain(pvMarshaller);
}

/*****************************************************************************/
/* Destroy marshallar and deinit driver
 * if SIGINT (ctrl +c)                                                       */
/*****************************************************************************/
void DeInitServer(void)
{
  DeinitMarshaller();
}

/*****************************************************************************/
/*! Translate cifX device handle to NX-API handle                            */
/*****************************************************************************/
static CIFXHANDLE GetNxApiHandle(uint32_t ulSerialNumber, uint32_t ulDeviceNumber)
{
  NXDRV_DEVICE_INFORMATION         tDeviceInfo      = {0};
  uint32_t                         ulSearchIdx      = 0;
  CIFXHANDLE                       hRet             = NULL;

  if (NXAPI_NO_ERROR == nxDrvFindDevice( NXDRV_FIND_FIRST, sizeof(tDeviceInfo), &tDeviceInfo, &ulSearchIdx))
  {
    if ( (tDeviceInfo.tSystemInfoBlock.ulSerialNumber == ulSerialNumber) &&
         (tDeviceInfo.tSystemInfoBlock.ulDeviceNumber == ulDeviceNumber)   )
    {
      hRet = tDeviceInfo.hDevice;
    } else
    {
      while (NXAPI_NO_ERROR == nxDrvFindDevice( NXDRV_FIND_NEXT, sizeof(tDeviceInfo), &tDeviceInfo, &ulSearchIdx))
      {
        if ( (tDeviceInfo.tSystemInfoBlock.ulSerialNumber == ulSerialNumber) &&
             (tDeviceInfo.tSystemInfoBlock.ulDeviceNumber == ulDeviceNumber)   )
        {
          hRet = tDeviceInfo.hDevice;
          break;
        }
      }
    }
  }
  
  return hRet;
}

/*****************************************************************************/
/*! File storage callback (uses NX-API)                                      */
/*****************************************************************************/
static int32_t HandleFileStorage(BOARD_INFORMATION* ptBoardInfo, 
                                 char* pszFileName, uint32_t ulFileSize, 
                                 uint8_t* pabFileData, uint32_t ulChannel, 
                                 uint32_t ulDownloadMode, void* pvUser)
{
  UNREFERENCED_PARAMETER(pvUser);

  int32_t    lRet   = 0;
  if (NXAPI_NO_ERROR == (lRet = nxDrvInit()))
  {
    CIFXHANDLE hNxApi = GetNxApiHandle(ptBoardInfo->tSystemInfo.ulSerialNumber, ptBoardInfo->tSystemInfo.ulDeviceNumber);

    if (NULL != hNxApi)
    {
      if (ulDownloadMode == DOWNLOAD_MODE_FIRMWARE)
        lRet = nxDrvDownload ( hNxApi, ulChannel, NXAPI_CMD_FIRMWARE, ulFileSize, pszFileName, pabFileData, NULL, NULL);
      else if (ulDownloadMode == DOWNLOAD_MODE_CONFIG)
        lRet = nxDrvDownload ( hNxApi, ulChannel, NXAPI_CMD_CONFIGURATION, ulFileSize, pszFileName, pabFileData, NULL, NULL);
      else
        lRet = CIFX_FILE_TYPE_INVALID;
    }
    else
    {
      lRet = CIFX_INVALID_HANDLE;
    }

    nxDrvExit();
  }

  return lRet;
}

/*****************************************************************************/
/*! Initialization the Marshallar                                            */
/*****************************************************************************/
TLR_RESULT InitMarshaller(void)
{
  HIL_MARSHALLER_PARAMS_T           tParams        = {{0}};
  HIL_MARSHALLER_CONNECTOR_PARAMS_T tTCPConnector  = {0};

  tTCPConnector.pfnConnectorInit = TCPConnectorInit;
  tTCPConnector.pvConfigData     = NULL;
  tTCPConnector.ulDataBufferCnt  = 5;
  tTCPConnector.ulDataBufferSize = 6000;
  tTCPConnector.ulTimeout        = 1000;

  TRANSPORT_LAYER_CONFIG_T          tCifXTransport = {0};
  CIFX_TRANSPORT_CONFIG             tCifXConfig    = {{0}};

  tCifXConfig.tDRVFunctions.pfnxDriverOpen                 = xDriverOpenWrapAda;
  tCifXConfig.tDRVFunctions.pfnxDriverClose                = xDriverCloseWrapAda;
  tCifXConfig.tDRVFunctions.pfnxDriverGetInformation       = xDriverGetInformationWrap;
  tCifXConfig.tDRVFunctions.pfnxDriverGetErrorDescription  = xDriverGetErrorDescription;
  tCifXConfig.tDRVFunctions.pfnxDriverEnumBoards           = xDriverEnumBoardsWrap;
  tCifXConfig.tDRVFunctions.pfnxDriverEnumChannels         = xDriverEnumChannelsWrap;
  tCifXConfig.tDRVFunctions.pfnxDriverMemoryPointer        = xDriverMemoryPointer;

  tCifXConfig.tDRVFunctions.pfnxSysdeviceOpen              = xSysdeviceOpenWrap;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceClose             = xSysdeviceCloseWrap;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceReset             = xSysdeviceReset;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceGetMBXState       = xSysdeviceGetMBXState;
  tCifXConfig.tDRVFunctions.pfnxSysdevicePutPacket         = xSysdevicePutPacketWrap;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceGetPacket         = xSysdeviceGetPacketWrap;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceDownload          = xSysdeviceDownload;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceInfo              = xSysdeviceInfoWrap;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceFindFirstFile     = xSysdeviceFindFirstFile;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceFindNextFile      = xSysdeviceFindNextFile;

  tCifXConfig.tDRVFunctions.pfnxChannelOpen                = xChannelOpenWrap;
  tCifXConfig.tDRVFunctions.pfnxChannelClose               = xChannelCloseWrap;
  tCifXConfig.tDRVFunctions.pfnxChannelDownload            = xChannelDownload;
  tCifXConfig.tDRVFunctions.pfnxChannelGetMBXState         = xChannelGetMBXStateWrapAda;
  tCifXConfig.tDRVFunctions.pfnxChannelPutPacket           = xChannelPutPacketWrapAda;
  tCifXConfig.tDRVFunctions.pfnxChannelGetPacket           = xChannelGetPacketWrapAda;
  tCifXConfig.tDRVFunctions.pfnxChannelGetSendPacket       = xChannelGetSendPacket;
  tCifXConfig.tDRVFunctions.pfnxChannelConfigLock          = xChannelConfigLock;
  tCifXConfig.tDRVFunctions.pfnxChannelReset               = xChannelReset;
  tCifXConfig.tDRVFunctions.pfnxChannelInfo                = xChannelInfoWrap;
  tCifXConfig.tDRVFunctions.pfnxChannelWatchdog            = xChannelWatchdog;
  tCifXConfig.tDRVFunctions.pfnxChannelHostState           = xChannelHostState;
  tCifXConfig.tDRVFunctions.pfnxChannelBusState            = xChannelBusState;
  tCifXConfig.tDRVFunctions.pfnxChannelIORead              = xChannelIOReadWrap;
  tCifXConfig.tDRVFunctions.pfnxChannelIOWrite             = xChannelIOWriteWrap;
  tCifXConfig.tDRVFunctions.pfnxChannelIOReadSendData      = xChannelIOReadSendData;
  tCifXConfig.tDRVFunctions.pfnxChannelControlBlock        = xChannelControlBlock;
  tCifXConfig.tDRVFunctions.pfnxChannelCommonStatusBlock   = xChannelCommonStatusBlockWrap;
  tCifXConfig.tDRVFunctions.pfnxChannelExtendedStatusBlock = xChannelExtendedStatusBlockWrap;

  tCifXConfig.tDRVFunctions.pfnxChannelPLCMemoryPtr        = xChannelPLCMemoryPtr;
  tCifXConfig.tDRVFunctions.pfnxChannelPLCIsReadReady      = xChannelPLCIsReadReady;
  tCifXConfig.tDRVFunctions.pfnxChannelPLCIsWriteReady     = xChannelPLCIsWriteReady;
  tCifXConfig.tDRVFunctions.pfnxChannelPLCActivateWrite    = xChannelPLCActivateWrite;
  tCifXConfig.tDRVFunctions.pfnxChannelPLCActivateRead     = xChannelPLCActivateRead;
  tCifXConfig.tDRVFunctions.pfnxChannelFindFirstFile       = xChannelFindFirstFile;
  tCifXConfig.tDRVFunctions.pfnxChannelFindNextFile        = xChannelFindNextFile;

  /* Install download hook */
  xDownloadHook_Install(&tCifXConfig.tDRVFunctions, HandleFileStorage, NULL);

  tCifXTransport.pfnInit  = cifXTransportInit;
  tCifXTransport.pvConfig = &tCifXConfig;

  tParams.ulMaxConnectors = 1;
  tParams.atTransports    = &tCifXTransport;
  tParams.ulTransportCnt  = 1;

  tParams.ptConnectors    = &tTCPConnector;
  tParams.ulConnectorCnt  = 1;

  TLR_RESULT eRet = HilMarshallerStart(&tParams, &g_pvMarshaller, MarshallerRequest, 0);

  return eRet;
}

/*****************************************************************************/
/* Destroy the Marshaller */
/*****************************************************************************/
void DeinitMarshaller(void)
{

  if(NULL != g_pvMarshaller)
    {
      A4A_Log("DeinitMarshaller", "Waiting for all processes to end...", A4A_LOG_LEVEL_INFO);
      HilMarshallerStop(g_pvMarshaller);
    }
}

int Initialise(void)
{
  A4A_Log("Initialise", "Marshaller initialization...", A4A_LOG_LEVEL_INFO);

  InitializeCriticalSection(&g_tcsQueueLock);
  
  if (TLR_S_OK == InitMarshaller())
    {
      A4A_Log("Initialise", "Marshaller initialised!", A4A_LOG_LEVEL_INFO);
    }
  else
    {
      A4A_Log("Initialise", "Marshaller initialization failed!", A4A_LOG_LEVEL_INFO);
    }

  return 0;
}

