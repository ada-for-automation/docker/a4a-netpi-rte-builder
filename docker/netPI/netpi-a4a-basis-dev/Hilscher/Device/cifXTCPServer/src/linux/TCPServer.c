/**************************************************************************************

   Copyright (c) Hilscher GmbH. All Rights Reserved.

 **************************************************************************************

   Filename:
    $Id: TCPServer.cpp 1206 2010-04-15 11:59:09Z sebastiand $
   Last Modification:
    $Author: sebastiand $
    $Date: 2010-04-15 13:59:09 +0200 (Thu, 15 Apr 2010) $
    $Revision: 1206 $

   Targets:
     Linux        : yes

   Description:
    Server handling

   Changes:

     Version   Date        Author   Description
     ----------------------------------------------------------------------------------

     2        16.11.2011   SD       add file download support (HandleFileStorage())
                                    add optional program arguments
                                     - port number specification
                                     - select and initialize only specified card
     1        02.06.2010   SD       initial version

**************************************************************************************/


#include <stdbool.h>
#include <stdio.h>

#include "OS_Includes.h"
#include "TCPConnector.h"
#include "TCPServer.h"
#include "cifxlinux.h"
#include "cifXDownloadHook.h"
#include <ifaddrs.h>

#include "a4a_cifXAPIWrap.h"
#include "a4a_LogWrapAda.h"
#include "a4a_cifXAPIWrapAda.h"

/* marshaller handle */
void*            g_pvMarshaller = NULL;
/* flag for main loop */
bool             g_fRunning     = 0;
/* flag to display once the network traffic */
bool             g_fTrafficOnce = 0;

unsigned short   g_usPortNumber = HIL_TRANSPORT_IP_PORT;

struct CIFX_LINUX_INIT  g_tInit = {0};

extern pthread_mutex_t* g_ptMutex;

/*****************************************************************************/
/*! Timer refreshing TCP traffic                                             */
/*****************************************************************************/
void TrafficTimer(void* dwUser)
{
  TCP_CONN_INTERNAL_T* ptTcpData = (TCP_CONN_INTERNAL_T*)dwUser;
  g_fTrafficOnce = 1;

  char Buffer [100];

  /* insert a handling to write down or display the traffic */

  sprintf(Buffer, "\nRX[Bytes]: %d, \nTX[Bytes]: %d\n",
          ptTcpData->ulRxCount,
          ptTcpData->ulTxCount);
  A4A_Log("TrafficTimer", Buffer, A4A_LOG_LEVEL_INFO);
}

/*****************************************************************************/
/*! This function is called after each interval specified in the setitimer
 *  function (see InitMarshaller) used to install a timer.
 *    \param iSignal   Signal which caused the call                          */
/*****************************************************************************/
void MarshallerTimer(void)
{
  HilMarshallerTimer(g_pvMarshaller);
}

/*****************************************************************************/
/*! Function for Marshaller Request                                          */
/*****************************************************************************/
void MarshallerRequest(void* pvMarshaller, void* pvUser)
{
  UNREFERENCED_PARAMETER(pvUser);
  HilMarshallerMain(pvMarshaller);
}

/*****************************************************************************/
/* Destroy marshallar and deinit driver
 * if SIGINT (ctrl +c)                                                       */
/*****************************************************************************/
void DeInitServer(void)
{
  DeinitMarshaller();

  OS_DELETELOCK(g_ptMutex);
  g_fRunning = 0;
}

/*****************************************************************************/
/*! File storage callback (uses NX-API)                                      */
/*****************************************************************************/
static int32_t HandleFileStorage(BOARD_INFORMATION* ptBoardInfo,
                                 char* pszFileName, uint32_t ulFileSize,
                                 uint8_t* pabFileData, uint32_t ulChannel,
                                 uint32_t ulDownloadMode, void* pvUser)
{
  int32_t          lRet   = CIFX_FUNCTION_FAILED;
  struct CIFX_LINUX_INIT* ptInit = (struct CIFX_LINUX_INIT*)(pvUser);
  char             abBaseDir[FILENAME_MAX];
  char             abFileDir[FILENAME_MAX];
  char             abFileName[FILENAME_MAX];
  FILE*            iFd;
  char Buffer [100];

  if (NULL != ptInit->base_dir)
  {
    OS_STRNCPY( abBaseDir, ptInit->base_dir, sizeof(ptInit->base_dir));
  }else
  {
    sprintf( abBaseDir, "/opt/cifx/deviceconfig");
  }

  /* check if configuration files are stored using slot number */
  if (ptBoardInfo->tSystemInfo.bDevIdNumber)
  {
    sprintf( abFileDir, "%s/Slot_%d/channel%d", abBaseDir, ptBoardInfo->tSystemInfo.bDevIdNumber, ulChannel);
  } else
  {
    sprintf( abFileDir, "%s/%d/%d/channel%d", abBaseDir, ptBoardInfo->tSystemInfo.ulDeviceNumber, ptBoardInfo->tSystemInfo.ulSerialNumber, ulChannel);
  }
  sprintf( abFileName, "%s/%s", abFileDir, pszFileName);

  sprintf(Buffer, "Store file: %s\n", abFileName);
  A4A_Log("HandleFileStorage", Buffer, A4A_LOG_LEVEL_ERROR);
  if ( NULL != (iFd = fopen( abFileName, "w+")))
  {
    int iFileSize;
    if ( 0 < (iFileSize = fwrite( pabFileData, 1, ulFileSize, iFd)))
    {
      if ((((unsigned int)iFileSize) == ulFileSize))
        lRet = CIFX_NO_ERROR;
    } else
    {
      sprintf(Buffer, "File storing failed %d\n", iFileSize);
      A4A_Log("HandleFileStorage", Buffer, A4A_LOG_LEVEL_ERROR);
    }
  } else
  {
    A4A_Log("HandleFileStorage", "File open failed!", A4A_LOG_LEVEL_ERROR);
    lRet = CIFX_FILE_OPEN_FAILED;
  }
  fclose( iFd);

  if (( DOWNLOAD_MODE_FIRMWARE == ulDownloadMode) && (lRet == CIFX_NO_ERROR))
  {
    /* if download succeeded restart device */
    xDriverRestartDevice ( NULL, ptBoardInfo->abBoardName, NULL);
  }

  return lRet;
}

/*****************************************************************************/
/*! Initialization the Marshaller                                         */
/*****************************************************************************/
TLR_RESULT InitMarshaller(void)
{
  HIL_MARSHALLER_PARAMS_T           tParams        = {{0}};
  HIL_MARSHALLER_CONNECTOR_PARAMS_T tTCPConnector  = {0};

  tTCPConnector.pfnConnectorInit = TCPConnectorInit;
  tTCPConnector.pvConfigData     = NULL;
  tTCPConnector.ulDataBufferCnt  = 1;
  tTCPConnector.ulDataBufferSize = 6000;
  tTCPConnector.ulTimeout        = 1000;

  TRANSPORT_LAYER_CONFIG_T          tCifXTransport = {0};
  CIFX_TRANSPORT_CONFIG             tCifXConfig    = {{0}};

  tCifXConfig.tDRVFunctions.pfnxDriverOpen                 = xDriverOpenWrapAda;
  tCifXConfig.tDRVFunctions.pfnxDriverClose                = xDriverCloseWrapAda;
  tCifXConfig.tDRVFunctions.pfnxDriverGetInformation       = xDriverGetInformationWrap;
  tCifXConfig.tDRVFunctions.pfnxDriverGetErrorDescription  = xDriverGetErrorDescription;
  tCifXConfig.tDRVFunctions.pfnxDriverEnumBoards           = xDriverEnumBoardsWrap;
  tCifXConfig.tDRVFunctions.pfnxDriverEnumChannels         = xDriverEnumChannelsWrap;
  tCifXConfig.tDRVFunctions.pfnxDriverMemoryPointer        = xDriverMemoryPointer;

  tCifXConfig.tDRVFunctions.pfnxSysdeviceOpen              = xSysdeviceOpenWrap;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceClose             = xSysdeviceCloseWrap;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceReset             = xSysdeviceReset;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceGetMBXState       = xSysdeviceGetMBXState;
  tCifXConfig.tDRVFunctions.pfnxSysdevicePutPacket         = xSysdevicePutPacketWrap;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceGetPacket         = xSysdeviceGetPacketWrap;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceDownload          = xSysdeviceDownload;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceInfo              = xSysdeviceInfoWrap;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceFindFirstFile     = xSysdeviceFindFirstFileWrap;
  tCifXConfig.tDRVFunctions.pfnxSysdeviceFindNextFile      = xSysdeviceFindNextFileWrap;

  tCifXConfig.tDRVFunctions.pfnxChannelOpen                = xChannelOpenWrap;
  tCifXConfig.tDRVFunctions.pfnxChannelClose               = xChannelCloseWrap;
  tCifXConfig.tDRVFunctions.pfnxChannelDownload            = xChannelDownload;
  tCifXConfig.tDRVFunctions.pfnxChannelGetMBXState         = xChannelGetMBXStateWrapAda;
  tCifXConfig.tDRVFunctions.pfnxChannelPutPacket           = xChannelPutPacketWrapAda;
  tCifXConfig.tDRVFunctions.pfnxChannelGetPacket           = xChannelGetPacketWrapAda;
  tCifXConfig.tDRVFunctions.pfnxChannelGetSendPacket       = xChannelGetSendPacket;
  tCifXConfig.tDRVFunctions.pfnxChannelConfigLock          = xChannelConfigLock;
  tCifXConfig.tDRVFunctions.pfnxChannelReset               = xChannelReset;
  tCifXConfig.tDRVFunctions.pfnxChannelInfo                = xChannelInfoWrap;
  tCifXConfig.tDRVFunctions.pfnxChannelWatchdog            = xChannelWatchdog;
  tCifXConfig.tDRVFunctions.pfnxChannelHostState           = xChannelHostState;
  tCifXConfig.tDRVFunctions.pfnxChannelBusState            = xChannelBusState;
  tCifXConfig.tDRVFunctions.pfnxChannelIORead              = xChannelIOReadWrap;
  tCifXConfig.tDRVFunctions.pfnxChannelIOWrite             = xChannelIOWriteWrap;
  tCifXConfig.tDRVFunctions.pfnxChannelIOReadSendData      = xChannelIOReadSendData;
  tCifXConfig.tDRVFunctions.pfnxChannelControlBlock        = xChannelControlBlock;
  tCifXConfig.tDRVFunctions.pfnxChannelCommonStatusBlock   = xChannelCommonStatusBlockWrap;
  tCifXConfig.tDRVFunctions.pfnxChannelExtendedStatusBlock = xChannelExtendedStatusBlockWrap;

  tCifXConfig.tDRVFunctions.pfnxChannelPLCMemoryPtr        = xChannelPLCMemoryPtr;
  tCifXConfig.tDRVFunctions.pfnxChannelPLCIsReadReady      = xChannelPLCIsReadReady;
  tCifXConfig.tDRVFunctions.pfnxChannelPLCIsWriteReady     = xChannelPLCIsWriteReady;
  tCifXConfig.tDRVFunctions.pfnxChannelPLCActivateWrite    = xChannelPLCActivateWrite;
  tCifXConfig.tDRVFunctions.pfnxChannelPLCActivateRead     = xChannelPLCActivateRead;
  tCifXConfig.tDRVFunctions.pfnxChannelFindFirstFile       = xChannelFindFirstFile;
  tCifXConfig.tDRVFunctions.pfnxChannelFindNextFile        = xChannelFindNextFile;

  /* Install download hook */
  xDownloadHook_Install(&tCifXConfig.tDRVFunctions, HandleFileStorage, &g_tInit);

  tCifXTransport.pfnInit  = cifXTransportInit;
  tCifXTransport.pvConfig = &tCifXConfig;

  tParams.ulMaxConnectors = 1;
  tParams.atTransports    = &tCifXTransport;
  tParams.ulTransportCnt  = 1;

  tParams.ptConnectors    = &tTCPConnector;
  tParams.ulConnectorCnt  = 1;

  TLR_RESULT eRet = HilMarshallerStart(&tParams, &g_pvMarshaller, MarshallerRequest, 0);

  return eRet;
}

/*****************************************************************************/
/* Destroy the Marshaller */
/*****************************************************************************/
void DeinitMarshaller()
{

  if(NULL != g_pvMarshaller)
    {
      A4A_Log("DeinitMarshaller", "Waiting for all process to end...", A4A_LOG_LEVEL_INFO);
      HilMarshallerStop(g_pvMarshaller);
    }
}


int Initialise()
{
  A4A_Log("Initialise", "Marshaller initialization...", A4A_LOG_LEVEL_INFO);

  /* set to default values */
  g_tInit.init_options        = CIFX_DRIVER_INIT_AUTOSCAN;
  g_tInit.iCardNumber         = 0;
  g_tInit.fEnableCardLocking  = 0;
  g_tInit.base_dir            = NULL;
  g_tInit.poll_interval       = 0;
  g_tInit.poll_StackSize      = 0;
  g_tInit.trace_level         = 255;
  g_tInit.user_card_cnt       = 0;
  g_ptMutex = OS_CREATELOCK();

  if (TLR_S_OK == InitMarshaller())
    {
      A4A_Log("Initialise", "Marshaller initialised!", A4A_LOG_LEVEL_INFO);
      g_fRunning = 1;
    }
  else
    {
      A4A_Log("Initialise", "Marshaller initialization failed!", A4A_LOG_LEVEL_INFO);
    }

  return 0;
}


