/*
-----------------------------------------------------------------------
--                       Ada for Automation                          --
--                                                                   --
--              Copyright (C) 2012-2014, Stephane LOS                --
--                                                                   --
-- This library is free software; you can redistribute it and/or     --
-- modify it under the terms of the GNU General Public               --
-- License as published by the Free Software Foundation; either      --
-- version 2 of the License, or (at your option) any later version.  --
--                                                                   --
-- This library is distributed in the hope that it will be useful,   --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of    --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU --
-- General Public License for more details.                          --
--                                                                   --
-- You should have received a copy of the GNU General Public         --
-- License along with this library; if not, write to the             --
-- Free Software Foundation, Inc., 59 Temple Place - Suite 330,      --
-- Boston, MA 02111-1307, USA.                                       --
--                                                                   --
-- As a special exception, if other files instantiate generics from  --
-- this unit, or you link this unit with other files to produce an   --
-- executable, this  unit  does not  by itself cause  the resulting  --
-- executable to be covered by the GNU General Public License. This  --
-- exception does not however invalidate any other reasons why the   --
-- executable file  might be covered by the  GNU Public License.     --
-----------------------------------------------------------------------
*/

#ifdef _WIN32
  #include <windows.h>
#endif

#include <stdio.h>

#include "cifXErrors.h"
#include "a4a_LogWrapAda.h"
#include "a4a_cifXAPIWrap.h"
#include "a4a_cifXAPIWrapAda.h"

#define WHAT_BUFFER_LEN        100

int32_t APIENTRY xDriverGetInformationWrap
  ( CIFXHANDLE  hDriver, uint32_t ulSize, void* pvDriverInfo)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];
  DRIVER_INFORMATION *pInfo = (DRIVER_INFORMATION *)pvDriverInfo;

  lRet = xDriverGetInformation( hDriver, ulSize, pvDriverInfo);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer,
            "\r\n"
            "Driver Version : %.32s\r\n"
            "ulBoardCnt     : %u",
            pInfo->abDriverVersion, pInfo->ulBoardCnt);
    A4A_Log("xDriverGetInformation", szWhatBuffer, A4A_LOG_LEVEL_INFO);

  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xDriverGetInformation", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t APIENTRY xDriverEnumBoardsWrap
  ( CIFXHANDLE  hDriver, uint32_t ulBoard, uint32_t ulSize, void* pvBoardInfo)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];
  BOARD_INFORMATION *pInfo = (BOARD_INFORMATION *)pvBoardInfo;

  lRet = xDriverEnumBoards( hDriver, ulBoard, ulSize, pvBoardInfo);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer,
            "Board %d\r\n"
            "abBoardName  : %.16s\r\n"
            "abBoardAlias : %.16s\r\n"
            "ulChannelCnt : %d",
            ulBoard,
            pInfo->abBoardName,
            pInfo->abBoardAlias,
            pInfo->ulChannelCnt);
    A4A_Log("xDriverEnumBoards", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xDriverEnumBoards", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t APIENTRY xDriverEnumChannelsWrap
  ( CIFXHANDLE  hDriver, uint32_t ulBoard, uint32_t ulChannel,
  uint32_t ulSize, void* pvChannelInfo)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];
  CHANNEL_INFORMATION *pInfo = (CHANNEL_INFORMATION *)pvChannelInfo;

  lRet = xDriverEnumChannels( hDriver, ulBoard, ulChannel,
                             ulSize, pvChannelInfo);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer,
            "Board %d, Channel %d\r\n"
            "abFWName : %.63s",
            ulBoard, ulChannel,
            pInfo->abFWName);
    A4A_Log("xDriverEnumChannels", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xDriverEnumChannels", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t APIENTRY xSysdeviceOpenWrap
  ( CIFXHANDLE  hDriver, char*   szBoard, CIFXHANDLE* phSysdevice)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];

  lRet = xSysdeviceOpen( hDriver, szBoard, phSysdevice);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer, "Opening Sysdevice %s", szBoard);
    A4A_Log("xSysdeviceOpen", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xSysdeviceOpen", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t APIENTRY xSysdeviceCloseWrap ( CIFXHANDLE  hSysdevice)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];

  lRet = xSysdeviceClose( hSysdevice);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer, "Closing Sysdevice");
    A4A_Log("xSysdeviceClose", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xSysdeviceClose", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t APIENTRY xSysdevicePutPacketWrap
  ( CIFXHANDLE  hSysdevice, CIFX_PACKET* ptSendPkt, uint32_t ulTimeout)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];

  lRet = xSysdevicePutPacket(hSysdevice, ptSendPkt, ulTimeout);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer, "Trace");
    A4A_Log("xSysdevicePutPacket", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xSysdevicePutPacket", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t APIENTRY xSysdeviceGetPacketWrap
  ( CIFXHANDLE  hSysdevice, uint32_t ulSize, CIFX_PACKET* ptRecvPkt,
  uint32_t ulTimeout)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];

  lRet = xSysdeviceGetPacket(hSysdevice, ulSize, ptRecvPkt, ulTimeout);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer, "Trace");
    A4A_Log("xSysdeviceGetPacket", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xSysdeviceGetPacket", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t APIENTRY xSysdeviceInfoWrap
( CIFXHANDLE  hSysdevice, uint32_t ulCmd, uint32_t ulSize, void* pvInfo)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];

  lRet = xSysdeviceInfo(hSysdevice, ulCmd, ulSize, pvInfo);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer, "Trace");
    A4A_Log("xSysdeviceInfo", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xSysdeviceInfo", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t     APIENTRY xSysdeviceFindFirstFileWrap
  ( CIFXHANDLE  hSysdevice, uint32_t ulChannel,
  CIFX_DIRECTORYENTRY* ptDirectoryInfo,
  PFN_RECV_PKT_CALLBACK pfnRecvPktCallback, void* pvUser)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];

  lRet = xSysdeviceFindFirstFile(hSysdevice, ulChannel, ptDirectoryInfo,
                                 pfnRecvPktCallback, pvUser);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer, "Trace : %s", ptDirectoryInfo->szFilename);
    A4A_Log("xSysdeviceFindFirstFile", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xSysdeviceFindFirstFile", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t     APIENTRY xSysdeviceFindNextFileWrap
  ( CIFXHANDLE  hSysdevice, uint32_t ulChannel,
  CIFX_DIRECTORYENTRY* ptDirectoryInfo,
  PFN_RECV_PKT_CALLBACK pfnRecvPktCallback, void* pvUser)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];

  lRet = xSysdeviceFindNextFile(hSysdevice, ulChannel, ptDirectoryInfo,
                                 pfnRecvPktCallback, pvUser);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer, "Trace : %s", ptDirectoryInfo->szFilename);
    A4A_Log("xSysdeviceFindNextFile", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xSysdeviceFindNextFile", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t APIENTRY xChannelOpenWrap
  ( CIFXHANDLE  hDriver,  char* szBoard, uint32_t ulChannel,
  CIFXHANDLE* phChannel)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];

  lRet = xChannelOpen( hDriver, szBoard, ulChannel, phChannel);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer,
            "Opening Channel %d of device %s", ulChannel, szBoard);
    A4A_Log("xChannelOpen", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xChannelOpen", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t APIENTRY xChannelCloseWrap ( CIFXHANDLE  hChannel)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];

  lRet = xChannelClose(hChannel);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer, "Closing channel");
    A4A_Log("xChannelClose", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xChannelClose", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t APIENTRY xChannelInfoWrap
  ( CIFXHANDLE  hChannel, uint32_t ulSize, void* pvChannelInfo)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];

  lRet = xChannelInfo(hChannel, ulSize, pvChannelInfo);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer, "Trace");
    A4A_Log("xChannelInfo", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xChannelInfo", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t APIENTRY xChannelIOReadWrap
  ( CIFXHANDLE  hChannel, uint32_t ulAreaNumber, uint32_t ulOffset,
  uint32_t ulDataLen, void* pvData, uint32_t ulTimeout)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];

  lRet = xChannelIORead(hChannel, ulAreaNumber, ulOffset,
                        ulDataLen, pvData, ulTimeout);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer, "Trace");
    A4A_Log("xChannelIORead", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xChannelIORead", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t APIENTRY xChannelIOWriteWrap
  ( CIFXHANDLE  hChannel, uint32_t ulAreaNumber, uint32_t ulOffset,
  uint32_t ulDataLen, void* pvData, uint32_t ulTimeout)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];

  lRet = xChannelIOWrite(hChannel, ulAreaNumber, ulOffset,
                         ulDataLen, pvData, ulTimeout);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer, "Trace");
    A4A_Log("xChannelIOWrite", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xChannelIOWrite", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t APIENTRY xChannelCommonStatusBlockWrap
  ( CIFXHANDLE  hChannel, uint32_t ulCmd, uint32_t ulOffset,
  uint32_t ulDataLen, void* pvData)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];

  lRet = xChannelCommonStatusBlock(hChannel, ulCmd, ulOffset,
                                   ulDataLen, pvData);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer, "Trace");
    A4A_Log("xChannelCommonStatusBlock", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xChannelCommonStatusBlock", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

int32_t APIENTRY xChannelExtendedStatusBlockWrap
  ( CIFXHANDLE  hChannel, uint32_t ulCmd, uint32_t ulOffset,
  uint32_t ulDataLen, void* pvData)
{
  int32_t lRet  = CIFX_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];

  lRet = xChannelExtendedStatusBlock(hChannel, ulCmd, ulOffset,
                                     ulDataLen, pvData);
  if (CIFX_NO_ERROR == lRet)
  {
    sprintf(szWhatBuffer, "Trace");
    A4A_Log("xChannelExtendedStatusBlock", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  }
  else
  {
    xDriverGetErrorDescription(lRet, szWhatBuffer, WHAT_BUFFER_LEN);
    A4A_Log("xChannelExtendedStatusBlock", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

